#pragma once

#include <cglm/types.h>
#include <raylib.h>

#include "vec.h"

Vector3 matrixTimesVector(Matrix m, Vector3 v);

typedef vec(Matrix) MatrixStack;

Matrix ConvertGlmMatrixToRaylib(mat4 matrix);
void ConvertRaylibMatrixToGlm(Matrix matrix, mat4 dest);

void MatrixStack_init(MatrixStack* stack);
void MatrixStack_deinit(MatrixStack* stack);
void MatrixStack_push(MatrixStack* stack, Matrix m);
void MatrixStack_pushGlm(MatrixStack* stack, mat4 m);
void MatrixStack_pushIdentity(MatrixStack* stack);
Matrix* MatrixStack_top(MatrixStack* stack);
Matrix MatrixStack_pop(MatrixStack* stack);
