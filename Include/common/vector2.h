#pragma once

#include <math.h>

#include <raylib.h>

Vector2 Vector2_add(Vector2 a, Vector2 b);
Vector2 Vector2_sub(Vector2 a, Vector2 b);
Vector2 Vector2_scale(Vector2 v, double s);
double Vector2_magnitude(Vector2 v);
Vector2 Vector2_setMagnitude(Vector2 v, double magnitude);
Vector2 Vector2_normalize(Vector2 v);
double Vector2_dist(Vector2 a, Vector2 b);
Vector2 Vector2_random(void);
Vector2 Vector2_rotate(Vector2 v, double angle);
