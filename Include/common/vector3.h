#pragma once

#include <raylib.h>

Vector3 Vector3_add(Vector3 a, Vector3 b);
Vector3 Vector3_scale(Vector3 v, double scale);
Vector3 Vector3_cross(Vector3 a, Vector3 b);
double Vector3_dot(Vector3 a, Vector3 b);
double Vector3_magnitude(Vector3 v);
Vector3 Vector3_normalize(Vector3 v);
Vector3 Vector3_sub(Vector3 a, Vector3 b);
Vector3 Vector3_random(void);
