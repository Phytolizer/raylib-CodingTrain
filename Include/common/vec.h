#pragma once

#include <math.h>
#include <stddef.h>
#include <stdlib.h>

#define vec(T) \
	struct \
	{ \
		T* ptr; \
		size_t length; \
		size_t capacity; \
	}

#define VEC_INIT_CAP_ 8

#define vec_init(V) \
	do \
	{ \
		(V)->ptr = malloc(VEC_INIT_CAP_ * sizeof(*(V)->ptr)); \
		(V)->length = 0; \
		(V)->capacity = VEC_INIT_CAP_; \
	} \
	while (0)

#define vec_reserve(V, Capacity) \
	do \
	{ \
		if (Capacity > (V)->capacity) \
		{ \
			while (Capacity > (V)->capacity) \
			{ \
				(V)->capacity *= 2; \
			} \
			(V)->ptr = realloc((V)->ptr, (V)->capacity * sizeof(*(V)->ptr)); \
		} \
	} \
	while (0)

#define vec_resize(V, Length) \
	do \
	{ \
		vec_reserve(V, Length); \
		(V)->length = Length; \
	} \
	while (0)

#define vec_push(V, Elem) \
	do \
	{ \
		vec_reserve(V, (V)->length + 1); \
		(V)->ptr[(V)->length] = Elem; \
		(V)->length++; \
	} \
	while (0)

#define vec_pop(V) ((V)->ptr[--(V)->length])

#define vec_clear(V) ((V)->length = 0)

#define vec_deinit(V) \
	do \
	{ \
		free((V)->ptr); \
		(V)->ptr = NULL; \
		(V)->length = 0; \
		(V)->capacity = 0; \
	} \
	while (0)

#define vec_last(V) (&(V)->ptr[(V)->length - 1])

#define vec_splice(V, I, N) \
	do \
	{ \
		for (long i = I; i < (V)->length - (N); ++i) \
		{ \
			(V)->ptr[i] = (V)->ptr[i + (N)]; \
		} \
		(V)->length -= (N); \
	} \
	while (0)
