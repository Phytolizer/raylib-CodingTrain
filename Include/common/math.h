#pragma once

#include <math.h>

double Radians(double degrees);
double Map(
        double x,
        double lower,
        double upper,
        double newLower,
        double newUpper);
