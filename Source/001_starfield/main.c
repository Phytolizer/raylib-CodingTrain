#include <stdio.h>
#include <stdlib.h>

#include <raylib.h>

#include "common/math.h"
#include "common/random.h"
#include "sodium/randombytes.h"

typedef struct Star
{
	double x;
	double y;
	double z;
} Star;

void Star_init(Star* self)
{
	self->x = random_between(-GetScreenWidth(), GetScreenWidth());
	self->y = random_between(-GetScreenHeight(), GetScreenHeight());
	self->z = random_between(0, GetScreenWidth());
}

void Star_update(Star* self)
{
	self->z--;
	if (self->z < 1)
	{
		self->z = GetScreenWidth();
		self->x = random_between(-GetScreenWidth(), GetScreenWidth());
		self->y = random_between(-GetScreenHeight(), GetScreenHeight());
	}
}

void Star_show(Star* self)
{
	double sx = Map(self->x / self->z, 0, 1, 0, GetScreenWidth());
	double sy = Map(self->y / self->z, 0, 1, 0, GetScreenHeight());
	sx += (double)GetScreenWidth() / 2;
	sy += (double)GetScreenHeight() / 2;
	double r = Map(self->z, 0, GetScreenWidth(), 8, 0);
	DrawCircle(sx, sy, r, WHITE);
}

#define STAR_COUNT 400

typedef struct State
{
	Star stars[STAR_COUNT];
} State;

void State_init(State* self)
{
	for (int i = 0; i < STAR_COUNT; i++)
	{
		Star_init(&self->stars[i]);
	}
}

void State_update(State* self)
{
	for (int i = 0; i < STAR_COUNT; i++)
	{
		Star_update(&self->stars[i]);
	}
}

void State_show(State* self)
{
	for (int i = 0; i < STAR_COUNT; ++i)
	{
		Star_show(&self->stars[i]);
	}
}

int main()
{
	InitWindow(400, 400, "Starfield");
	State state;
	State_init(&state);

	while (!WindowShouldClose())
	{
		BeginDrawing();
		ClearBackground(BLACK);

		State_update(&state);
		State_show(&state);

		EndDrawing();
	}

	CloseWindow();

	return EXIT_SUCCESS;
}
