#include <stdio.h>
#include <stdlib.h>

#include <cglm/cglm.h>
#include <common/vec.h>
#include <common/vector2.h>
#include <common/vector3.h>
#include <raylib.h>
#include <sodium/randombytes.h>

#include "common/matrix.h"

static void DumpVector3(Vector3 v)
{
	printf("V3{%10.2f %10.2f %10.2f}", v.x, v.y, v.z);
}

static void DumpVector3s(Vector3* vs, size_t count)
{
	fputs("[", stdout);
	bool first = true;
	for (size_t i = 0; i < count; ++i)
	{
		if (first)
		{
			first = false;
		}
		else
		{
			fputs(", ", stdout);
		}
		DumpVector3(vs[i]);
	}
	fputs("]", stdout);
}

static Vector3* RaycastPlane(Ray r, Ray u, Ray v)
{
	Vector3 n = Vector3_normalize(Vector3_cross(u.direction, v.direction));
	double d = Vector3_dot(Vector3_sub(u.position, r.position), n);
	double nd = Vector3_dot(r.direction, n);

	if (nd >= 0)
	{
		return NULL;
	}

	double t = d / nd;
	Vector3* result = malloc(sizeof(Vector3));
	*result = Vector3_add(
	        r.position,
	        Vector3_scale(Vector3_normalize(r.direction), t));
	return result;
}

static Vector3* RaycastTriangle(Ray r, Vector3 a, Vector3 b, Vector3 c)
{
	Vector3 u = Vector3_sub(b, a);
	Vector3 v = Vector3_sub(c, a);
	Vector3* planar = RaycastPlane(
	        r,
	        (Ray){.position = a, .direction = u},
	        (Ray){.position = a, .direction = v});
	if (planar == NULL)
	{
		return NULL;
	}

	Vector3 w = Vector3_sub(*planar, a);
	double uu = Vector3_dot(u, u);
	double uv = Vector3_dot(u, v);
	double vv = Vector3_dot(v, v);
	double wu = Vector3_dot(w, u);
	double wv = Vector3_dot(w, v);
	double denom = uu * vv - uv * uv;
	double alpha = (vv * wu - uv * wv) / denom;
	double beta = (uu * wv - uv * wu) / denom;
	if (alpha < 0 || alpha > 1 || beta < 0 || beta > 1)
	{
		free(planar);
		return NULL;
	}

	return planar;
}

static Vector3* RaycastSquare(
        Ray r,
        Vector3 corner1,
        Vector3 corner2,
        Vector3 corner3,
        Vector3 corner4)
{
	Vector3* result = RaycastTriangle(r, corner1, corner2, corner3);
	if (result)
	{
		return result;
	}
	result = RaycastTriangle(r, corner4, corner2, corner3);
	if (result)
	{
		return result;
	}
	return NULL;
}

static Ray PixelRay(Vector2 pixel, Camera camera)
{
	double ndcX = (pixel.x + 0.5) / GetScreenWidth();
	double ndcY = (pixel.y + 0.5) / GetScreenHeight();
	double screenX = 2 * ndcX - 1;
	double screenY = 1 - 2 * ndcY;
	double aspectRatio = (double)GetScreenWidth() / GetScreenHeight();
	double tanFovOver2 = tan(camera.fovy * DEG2RAD / 2);
	double cameraX = (2 * screenX - 1) * aspectRatio * tanFovOver2;
	double cameraY = (1 - 2 * screenY) * tanFovOver2;
	Vector3 cameraPixel = {
	        .x = cameraX,
	        .y = cameraY,
	        .z = -1.0,
	};
	Ray cameraRay = {
	        .position = (Vector3){0, 0, 0},
	        .direction = Vector3_normalize(cameraPixel),
	};
	Matrix viewMatrix = GetCameraMatrix(camera);
	vec4 viewMatrixV[4];
	viewMatrixV[0][0] = viewMatrix.m0;
	viewMatrixV[0][1] = viewMatrix.m1;
	viewMatrixV[0][2] = viewMatrix.m2;
	viewMatrixV[0][3] = viewMatrix.m3;
	viewMatrixV[1][0] = viewMatrix.m4;
	viewMatrixV[1][1] = viewMatrix.m5;
	viewMatrixV[1][2] = viewMatrix.m6;
	viewMatrixV[1][3] = viewMatrix.m7;
	viewMatrixV[2][0] = viewMatrix.m8;
	viewMatrixV[2][1] = viewMatrix.m9;
	viewMatrixV[2][2] = viewMatrix.m10;
	viewMatrixV[2][3] = viewMatrix.m11;
	viewMatrixV[3][0] = viewMatrix.m12;
	viewMatrixV[3][1] = viewMatrix.m13;
	viewMatrixV[3][2] = viewMatrix.m14;
	viewMatrixV[3][3] = viewMatrix.m15;
	vec4 cameraMatrixV[4];
	glm_mat4_inv(viewMatrixV, cameraMatrixV);
	float cameraPositionV[3] = {
	        camera.position.x,
	        camera.position.y,
	        camera.position.z,
	};
	glm_translate(cameraMatrixV, cameraPositionV);
	Matrix cameraMatrix = {
	        .m0 = cameraMatrixV[0][0],
	        .m1 = cameraMatrixV[0][1],
	        .m2 = cameraMatrixV[0][2],
	        .m3 = cameraMatrixV[0][3],
	        .m4 = cameraMatrixV[1][0],
	        .m5 = cameraMatrixV[1][1],
	        .m6 = cameraMatrixV[1][2],
	        .m7 = cameraMatrixV[1][3],
	        .m8 = cameraMatrixV[2][0],
	        .m9 = cameraMatrixV[2][1],
	        .m10 = cameraMatrixV[2][2],
	        .m11 = cameraMatrixV[2][3],
	        .m12 = cameraMatrixV[3][0],
	        .m13 = cameraMatrixV[3][1],
	        .m14 = cameraMatrixV[3][2],
	        .m15 = cameraMatrixV[3][3],
	};
	Ray result = {
	        .position = matrixTimesVector(cameraMatrix, cameraRay.position),
	        .direction = matrixTimesVector(cameraMatrix, cameraRay.direction),
	};
	result.position = camera.position;
	DumpVector3(result.direction);
	printf("\n");
	return result;
}

static void DrawCubeFace(
        Vector3 corner1,
        Vector3 corner2,
        Vector3 corner3,
        Vector3 corner4,
        Color color)
{
	static Vector3 points[4] = {0};
	points[0] = corner1;
	points[1] = corner2;
	points[2] = corner3;
	points[3] = corner4;
	DrawTriangleStrip3D(points, 4, color);
	DrawLine3D(points[0], points[1], BLACK);
	DrawLine3D(points[0], points[2], BLACK);
	DrawLine3D(points[2], points[3], BLACK);
	DrawLine3D(points[1], points[3], BLACK);
}

typedef struct BoxFace
{
	Ray axes[2];
	Color color;
	Color brighter;
	bool highlighted;
} BoxFace;

void BoxFace_show(BoxFace* face)
{
	DrawCubeFace(
	        face->axes[0].position,
	        Vector3_add(face->axes[0].position, face->axes[0].direction),
	        Vector3_add(face->axes[1].position, face->axes[1].direction),
	        Vector3_add(
	                Vector3_add(
	                        face->axes[0].position,
	                        face->axes[0].direction),
	                face->axes[1].direction),
	        face->highlighted ? face->brighter : face->color);
}

typedef struct Box
{
	Vector3 position;
	vec(BoxFace) faces;
	float len;
} Box;

void Box_show(Box* box)
{
	for (long i = 0; i < box->faces.length; ++i)
	{
		BoxFace_show(&box->faces.ptr[i]);
	}
}

#define CAMERA_MOUSE_MOVE_SENSITIVITY 0.01
#define CAMERA_MOUSE_SCROLL_SENSITIVITY 1
#define CAMERA_MINIMUM_TARGET_DISTANCE 30
#define CAMERA_MAXIMUM_TARGET_DISTANCE 100

#define DIM 3
#define LEN 10

Box* boxen;

#define XAXIS ((Vector3){LEN, 0, 0})
#define YAXIS ((Vector3){0, LEN, 0})
#define ZAXIS ((Vector3){0, 0, LEN})
#define RANDOM_COLOR \
	((Color){ \
	        .r = randombytes_uniform(255), \
	        .g = randombytes_uniform(255), \
	        .b = randombytes_uniform(255), \
	        .a = 255, \
	})

static void handleInput(Camera* camera)
{
	static bool init = false;
	static Vector2 previousMousePosition = {0.0F, 0.0F};
	static Vector2 cameraAngle = {0.0F, 0.0F};
	static float targetDistance = 50;

	Vector2 mousePosition = GetMousePosition();
	float mouseWheelMove = GetMouseWheelMove();

	bool rmb = IsMouseButtonDown(MOUSE_RIGHT_BUTTON);

	Vector2 mousePositionDelta
	        = Vector2_sub(mousePosition, previousMousePosition);
	previousMousePosition = mousePosition;

	targetDistance += mouseWheelMove * CAMERA_MOUSE_SCROLL_SENSITIVITY;
	if (targetDistance < CAMERA_MINIMUM_TARGET_DISTANCE)
	{
		targetDistance = CAMERA_MINIMUM_TARGET_DISTANCE;
	}
	if (targetDistance > CAMERA_MAXIMUM_TARGET_DISTANCE)
	{
		targetDistance = CAMERA_MAXIMUM_TARGET_DISTANCE;
	}

	if (rmb)
	{
		cameraAngle.x += mousePositionDelta.x * CAMERA_MOUSE_MOVE_SENSITIVITY;
		cameraAngle.y += mousePositionDelta.y * CAMERA_MOUSE_MOVE_SENSITIVITY;
	}
	camera->position.x
	        = -sin(cameraAngle.x) * targetDistance * cos(cameraAngle.y)
	        + camera->target.x;
	camera->position.y
	        = -sin(cameraAngle.y) * targetDistance + camera->target.y;
	camera->position.z
	        = -cos(cameraAngle.x) * targetDistance * cos(cameraAngle.y)
	        + camera->target.z;

	Ray mouseRay = GetMouseRay(mousePosition, *camera);
	for (int i = 0; i < DIM * DIM * DIM; ++i)
	{
		Box* box = &boxen[i];
		for (int j = 0; j < box->faces.length; ++j)
		{
			BoxFace* face = &box->faces.ptr[j];
			Vector3* cast = RaycastSquare(
			        mouseRay,
			        face->axes[0].position,
			        Vector3_add(
			                face->axes[0].position,
			                face->axes[0].direction),
			        Vector3_add(
			                face->axes[1].position,
			                face->axes[1].direction),
			        Vector3_add(
			                Vector3_add(
			                        face->axes[0].position,
			                        face->axes[0].direction),
			                face->axes[1].direction));
			if (cast)
			{
				face->highlighted = true;
				free(cast);
			}
			else
			{
				face->highlighted = false;
			}
		}
	}
}

void setup()
{
	boxen = malloc(DIM * DIM * DIM * sizeof(Box));
	for (int i = 0; i < DIM; i++)
	{
		for (int j = 0; j < DIM; j++)
		{
			for (int k = 0; k < DIM; k++)
			{
				Box* box = &boxen[i + j * DIM + k * DIM * DIM];
				box->position = (Vector3){i * LEN, j * LEN, k * LEN};
				box->len = LEN;
				vec_init(&box->faces);
				if (i == 0)
				{
					vec_push(&box->faces, ((BoxFace){
              .axes = {
                (Ray){
                  .position = box->position,
                  .direction = ZAXIS,
                },
                (Ray){
                  .position = box->position,
                  .direction = YAXIS,
                },
              },
              .color = RED,
              .brighter = (Color){255, 128, 128, 255},
          }));
				}
				else if (i == DIM - 1)
				{
					vec_push(&box->faces, ((BoxFace){
              .axes = {
                (Ray){
                  .position = Vector3_add(box->position, Vector3_scale(XAXIS, i - 1)),
                  .direction = YAXIS,
                },
                (Ray){
                  .position = Vector3_add(box->position, Vector3_scale(XAXIS, i - 1)),
                  .direction = ZAXIS,
                },
              },
              .color = ORANGE,
              .brighter = (Color){255, 200, 100, 255},
          }));
				}
				if (j == 0)
				{
					vec_push(
              &box->faces,
              ((BoxFace){
                  .axes = {
                    (Ray){
                      .position = box->position,
                      .direction = XAXIS,
                    },
                    (Ray){
                      .position = box->position,
                      .direction = ZAXIS,
                    },
                  },
                  .color = GREEN,
                  .brighter = (Color){0, 255, 0, 255},
              }));
				}
				else if (j == DIM - 1)
				{
					vec_push(
              &box->faces,
              ((BoxFace){
                  .axes = {
                    (Ray){
                      .position = Vector3_add(box->position, Vector3_scale(YAXIS, j - 1)),
                      .direction = ZAXIS,
                    },
                    (Ray){
                      .position = Vector3_add(box->position, Vector3_scale(YAXIS, j - 1)),
                      .direction = XAXIS,
                    },
                  },
                  .color = BLUE,
                  .brighter = (Color){0, 0, 255, 255},
              }));
				}
				if (k == 0)
				{
					vec_push(&box->faces, ((BoxFace){
              .axes = {
                (Ray){
                  .position = box->position,
                  .direction = YAXIS,
                },
                (Ray){
                  .position = box->position,
                  .direction = XAXIS,
                },
              },
              .color = WHITE,
              .brighter = (Color){192, 192, 192, 255},
          }));
				}
				else if (k == DIM - 1)
				{
					vec_push(&box->faces, ((BoxFace){
              .axes = {
                (Ray){
                  .position = Vector3_add(box->position, Vector3_scale(ZAXIS, k - 1)),
                  .direction = XAXIS,
                },
                (Ray){
                  .position = Vector3_add(box->position, Vector3_scale(ZAXIS, k - 1)),
                  .direction = YAXIS,
                },
              },
              .color = YELLOW,
              .brighter = (Color){255, 255, 128, 255},
          }));
				}
			}
		}
	}
}

void draw()
{
	ClearBackground((Color){51, 51, 51, 255});
	for (int i = 0; i < DIM * DIM * DIM; i++)
	{
		Box_show(&boxen[i]);
	}
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(400, 400, "Rubik's Cube");
	SetWindowState(
	        FLAG_WINDOW_HIGHDPI | FLAG_VSYNC_HINT | FLAG_WINDOW_RESIZABLE);

	setup();
	Camera camera = {
	        .position = {0, 0, 0},
	        .target = {15, 15, 15},
	        .up = {0, 1, 0},
	        .fovy = 90,
	        .projection = CAMERA_PERSPECTIVE,
	};
	SetCameraMode(camera, CAMERA_CUSTOM);

	while (!WindowShouldClose())
	{
		handleInput(&camera);
		BeginDrawing();
		BeginMode3D(camera);
		draw();
		EndMode3D();
		DrawFPS(10, 10);
		EndDrawing();
	}

	CloseWindow();
	free(boxen);
	return 0;
}
