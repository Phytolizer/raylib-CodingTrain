#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <common/vec.h>
#include <common/vector2.h>
#include <raylib.h>
#include <sodium.h>

#include "common/math.h"

typedef struct Boundary_t
{
	Vector2 a;
	Vector2 b;
} Boundary_t;

static Boundary_t Boundary_new(double x1, double y1, double x2, double y2)
{
	return (Boundary_t){
	        .a = (Vector2){x1, y1},
	        .b = (Vector2){x2, y2},
	};
}

static void Boundary_show(const Boundary_t* b)
{
	Color c = WHITE;
	DrawLineV(b->a, b->b, c);
}

typedef struct Ray_t
{
	Vector2 pos;
	Vector2 dir;
} Ray_t;

static Ray_t Ray_new(Vector2 pos, double dir)
{
	return (Ray_t){
	        .pos = pos,
	        .dir = (Vector2){cos(dir), sin(dir)},
	};
}

static void Ray_lookAt(Ray_t* r, double x, double y)
{
	r->dir.x = x - r->pos.x;
	r->dir.y = y - r->pos.y;
	r->dir = Vector2_normalize(r->dir);
}

static void Ray_show(const Ray_t* r)
{
	DrawLineV(
	        r->pos,
	        Vector2_add(r->pos, Vector2_scale(r->dir, 10)),
	        (Color){
	                .r = 255,
	                .g = 255,
	                .b = 255,
	                .a = 100,
	        });
}

static Vector2* Ray_cast(const Ray_t* r, const Boundary_t* b)
{
	double x1 = b->a.x;
	double y1 = b->a.y;
	double x2 = b->b.x;
	double y2 = b->b.y;

	double x3 = r->pos.x;
	double y3 = r->pos.y;
	double x4 = r->pos.x + r->dir.x;
	double y4 = r->pos.y + r->dir.y;

	double den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
	if (den == 0)
	{
		return NULL;
	}

	double t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
	if (t < 0 || t > 1)
	{
		return NULL;
	}

	double u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;
	if (u < 0)
	{
		return NULL;
	}
	Vector2* pt = malloc(sizeof(Vector2));
	pt->x = x1 + t * (x2 - x1);
	pt->y = y1 + t * (y2 - y1);
	return pt;
}

typedef struct Particle_t
{
	Vector2 pos;
	vec(Ray_t) rays;
} Particle_t;

static Particle_t Particle_new()
{
	int width = GetScreenWidth();
	int height = GetScreenHeight();
	Particle_t p = {
      .pos = (Vector2){
          .x = (double)width / 2, 
          .y = (double)height / 2,
      },
      .rays = {},
  };
	vec_init(&p.rays);
	for (int i = 0; i < 360; i += 1)
	{
		vec_push(&p.rays, Ray_new(p.pos, Radians(i)));
	}
	return p;
}

static void Particle_deinit(Particle_t* p)
{
	vec_deinit(&p->rays);
}

static void Particle_update(Particle_t* p)
{
	p->pos = GetMousePosition();
	for (size_t i = 0; i < p->rays.length; ++i)
	{
		p->rays.ptr[i].pos = p->pos;
	}
}

static void Particle_show(Particle_t* p)
{
	DrawCircle(p->pos.x, p->pos.y, 4, WHITE);
	for (size_t i = 0; i < p->rays.length; ++i)
	{
		Ray_show(&p->rays.ptr[i]);
	}
}

typedef vec(Boundary_t) Boundaries_t;

static void Particle_look(Particle_t* p, Boundaries_t* walls)
{
	for (int i = 0; i < p->rays.length; ++i)
	{
		Ray_t* ray = &p->rays.ptr[i];
		double record = INFINITY;
		Vector2* closest = NULL;
		for (int j = 0; j < walls->length; ++j)
		{
			Vector2* pt = Ray_cast(ray, &walls->ptr[j]);
			if (pt)
			{
				double d = Vector2_dist(p->pos, *pt);
				if (d < record)
				{
					record = d;
					closest = pt;
				}
				else
				{
					free(pt);
				}
			}
		}
		if (closest)
		{
			DrawLineV(p->pos, *closest, WHITE);
			free(closest);
		}
	}
}

static Boundaries_t walls;
static Particle_t particle;
static double xoff = 0;
static double yoff = 0;

static void setup()
{
	int width = GetScreenWidth();
	int height = GetScreenHeight();
	vec_init(&walls);
	vec_push(&walls, Boundary_new(0, 0, width, 0));
	vec_push(&walls, Boundary_new(width, 0, width, height));
	vec_push(&walls, Boundary_new(width, height, 0, height));
	vec_push(&walls, Boundary_new(0, height, 0, 0));
	for (int i = 0; i < 5; ++i)
	{
		double x1 = randombytes_uniform(width);
		double y1 = randombytes_uniform(height);
		double x2 = randombytes_uniform(width);
		double y2 = randombytes_uniform(height);
		vec_push(&walls, Boundary_new(x1, y1, x2, y2));
	}
	particle = Particle_new();
}

static void draw()
{
	Vector2 mouse = GetMousePosition();
	for (size_t i = 0; i < walls.length; ++i)
	{
		Boundary_show(&walls.ptr[i]);
	}
	Particle_update(&particle);
	Particle_show(&particle);
	Particle_look(&particle, &walls);
}

int main(int argc, char** argv)
{
	InitWindow(400, 400, "Raycasting!");

	setup();

	while (!WindowShouldClose())
	{
		BeginDrawing();
		ClearBackground(BLACK);

		draw();

		EndDrawing();
	}

	Particle_deinit(&particle);
	vec_deinit(&walls);
	CloseWindow();
}
