#include <common/random.h>
#include <raylib.h>

#include "common/math.h"

#define NDROPS 500

typedef struct
{
	double x;
	double y;
	double z;
	double yspeed;
	double length;
} Drop;

void Drop_init(Drop* d)
{
	d->x = random_between(0, GetScreenWidth());
	d->y = random_between(-200, -100);
	d->z = random_between(0, 20);
	d->yspeed = Map(d->z, 0, 20, 10, 20);
	d->length = Map(d->z, 0, 20, 10, 20);
}

void Drop_fall(Drop* d)
{
	d->y += d->yspeed;
	double grav = Map(d->z, 0, 20, 0, 0.2);
	d->yspeed += grav;

	if (d->y > GetScreenHeight())
	{
		Drop_init(d);
	}
}

void Drop_show(Drop* d)
{
	DrawLineEx(
	        CLITERAL(Vector2){d->x, d->y},
	        CLITERAL(Vector2){d->x, d->y + 10},
	        Map(d->z, 0, 20, 1, 3),
	        CLITERAL(Color){138, 43, 226, 255});
}

int main()
{
	InitWindow(640, 360, "Purple Rain");
	Drop d[NDROPS];
	for (int i = 0; i < NDROPS; i++)
	{
		Drop_init(&d[i]);
	}
	double timer = 0;

	while (!WindowShouldClose())
	{
		timer += GetFrameTime();
		if (timer >= 1.0 / 60.0)
		{
			for (int i = 0; i < NDROPS; ++i)
			{
				Drop_fall(&d[i]);
			}
			timer = 0;
		}
		BeginDrawing();
		ClearBackground(CLITERAL(Color){230, 230, 250, 255});
		for (int i = 0; i < NDROPS; ++i)
		{
			Drop_show(&d[i]);
		}
		EndDrawing();
	}

	CloseWindow();
}
