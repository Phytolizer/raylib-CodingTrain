#include <raylib.h>
#include <sodium.h>

static double lerp(double a, double b, double t)
{
	return a + (b - a) * t;
}

struct RGB_t
{
	double r;
	double g;
	double b;
};

struct HSV_t
{
	double h;
	double s;
	double v;
};

struct RGB_t HsvToRgb(struct HSV_t hsv)
{
	if (hsv.s == 0)
	{
		return (struct RGB_t){
		        .r = hsv.v * 255,
		        .g = hsv.v * 255,
		        .b = hsv.v * 255,
		};
	}

	double h = hsv.h * 6;
	if (h == 6)
	{
		h = 0;
	}
	int i = h;
	double v1 = hsv.v * (1 - hsv.s);
	double v2 = hsv.v * (1 - hsv.s * (h - i));
	double v3 = hsv.v * (1 - hsv.s * (1 - (h - i)));

	struct RGB_t rgb = {0};
	switch (i)
	{
		case 0:
			rgb.r = hsv.v;
			rgb.g = v3;
			rgb.b = v1;
			break;
		case 1:
			rgb.r = v2;
			rgb.g = hsv.v;
			rgb.b = v1;
			break;
		case 2:
			rgb.r = v1;
			rgb.g = hsv.v;
			rgb.b = v3;
			break;
		case 3:
			rgb.r = v1;
			rgb.g = v2;
			rgb.b = hsv.v;
			break;
		case 4:
			rgb.r = v3;
			rgb.g = v1;
			rgb.b = hsv.v;
			break;
		default:
			rgb.r = hsv.v;
			rgb.g = v1;
			rgb.b = v2;
			break;
	}
	return (struct RGB_t){
	        .r = rgb.r * 255,
	        .g = rgb.g * 255,
	        .b = rgb.b * 255,
	};
}

typedef struct Particle_t
{
	double x;
	double y;
	double dx;
	double dy;
} Particle_t;

static Particle_t Particle_new(double x, double y)
{
	double dx = randombytes_uniform(1000);
	if (dx > 500)
	{
		dx = -dx;
	}
	dx /= 500;
	dx *= 3;
	double dy = randombytes_uniform(1000);
	if (dy > 500)
	{
		dy = -dy;
	}
	dy /= 500;
	dy *= 3;
	return (Particle_t){
	        .x = x,
	        .y = y,
	        .dx = dx,
	        .dy = dy,
	};
}

static void Particle_update(Particle_t* p, int width, int height)
{
	p->x += p->dx;
	p->y += p->dy;

	if (p->x < 0 || p->x >= width)
	{
		p->dx = -p->dx;
	}
	if (p->y < 0 || p->y >= height)
	{
		p->dy = -p->dy;
	}
}

static Vector2 Particle_as_vector(Particle_t* p)
{
	return (Vector2){
	        .x = p->x,
	        .y = p->y,
	};
}

static Vector2 Quadratic(
        Vector2 p0,
        Vector2 p1,
        Vector2 p2,
        Color color,
        double t)
{
	double x1 = lerp(p0.x, p1.x, t);
	double y1 = lerp(p0.y, p1.y, t);
	double x2 = lerp(p1.x, p2.x, t);
	double y2 = lerp(p1.y, p2.y, t);
	DrawLineEx((Vector2){x1, y1}, (Vector2){x2, y2}, 4, color);
	double x = lerp(x1, x2, t);
	double y = lerp(y1, y2, t);
	return (Vector2){x, y};
}

static Vector2 Cubic(
        Vector2 p0,
        Vector2 p1,
        Vector2 p2,
        Vector2 p3,
        Color color,
        double t)
{
	Vector2 v1 = Quadratic(p0, p1, p2, color, t);
	Vector2 v2 = Quadratic(p1, p2, p3, color, t);
	double x = lerp(v1.x, v2.x, t);
	double y = lerp(v1.y, v2.y, t);
	DrawLineEx(v1, v2, 4, color);
	return (Vector2){x, y};
}

#define DELTA 0.05
#define COUNT (1.0 / DELTA)

int main(int argc, char* argv[])
{
	const int width = 600;
	const int height = 600;
	InitWindow(width, height, "Bezier curve");
	SetTargetFPS(60);
	int frameCount = 0;

	Particle_t p0 = Particle_new(0, 300);
	Particle_t p1 = Particle_new(300, 0);
	Particle_t p2 = Particle_new(400, 0);
	Particle_t p3 = Particle_new(600, 300);

	while (!WindowShouldClose())
	{
		Vector2 mouse = GetMousePosition();
		BeginDrawing();
		ClearBackground(BLACK);

		// p1.x = mouse.x;
		// p1.y = mouse.y;

		// DrawLineEx(p0, p1, 4, WHITE);
		Vector2 prev = Particle_as_vector(&p0);
		for (int i = 0; i <= COUNT; ++i)
		{
			double t = i * DELTA;
			struct RGB_t rgb = HsvToRgb((struct HSV_t){
			        .h = t,
			        .s = 1,
			        .v = 1,
			});
			Color color = {
			        .r = rgb.r,
			        .g = rgb.g,
			        .b = rgb.b,
			        .a = 128,
			};

			Vector2 v
			        = Cubic(Particle_as_vector(&p0),
			                Particle_as_vector(&p1),
			                Particle_as_vector(&p2),
			                Particle_as_vector(&p3),
			                color,
			                t);
			DrawLineEx(prev, v, 4, color);
			prev = v;
		}
		Particle_update(&p1, width, height);
		Particle_update(&p2, width, height);

		EndDrawing();
	}

	CloseWindow();

	return 0;
}
