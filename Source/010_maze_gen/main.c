#include <assert.h>
#include <stddef.h>
#include <stdint.h>

#include <common/vec.h>
#include <raylib.h>
#include <sodium/randombytes.h>

#define W 40
#define WIDTH 400
#define HEIGHT 400
#define COLS (WIDTH / W)
#define ROWS (HEIGHT / W)

#define PURPLISH \
	CLITERAL(Color) \
	{ \
		255, 0, 255, 100 \
	}
#define BLUEISH \
	CLITERAL(Color) \
	{ \
		0, 0, 255, 100 \
	}

typedef enum
{
	WALL_TOP,
	WALL_RIGHT,
	WALL_BOTTOM,
	WALL_LEFT,
	WALL_COUNT,
} Wall;

typedef struct Cell
{
	int32_t i;
	int32_t j;
	bool walls[WALL_COUNT];
	bool visited;
	bool highlighted;
} Cell;

typedef struct ChosenCell
{
	Cell* cell;
	Wall side;
} ChosenCell;

int32_t indexOf(int32_t i, int32_t j)
{
	return i + j * COLS;
}

Cell* getAt(Cell* grid, int32_t i, int32_t j)
{
	if (i < 0 || j < 0 || i >= COLS || j >= ROWS)
	{
		return NULL;
	}
	return &grid[indexOf(i, j)];
}

void Cell_init(Cell* self, int32_t i, int32_t j)
{
	self->i = i;
	self->j = j;
	self->walls[WALL_TOP] = true;
	self->walls[WALL_RIGHT] = true;
	self->walls[WALL_BOTTOM] = true;
	self->walls[WALL_LEFT] = true;
	self->visited = false;
	self->highlighted = false;
}

void Cell_show(Cell* self)
{
	double x = self->i * W;
	double y = self->j * W;
	if (self->visited)
	{
		DrawRectangle(x, y, W, W, PURPLISH);
	}
	if (self->highlighted)
	{
		DrawRectangle(x, y, W, W, BLUEISH);
	}
	if (self->walls[WALL_TOP])
	{
		DrawLine(x, y, x + W, y, WHITE);
	}
	if (self->walls[WALL_RIGHT])
	{
		DrawLine(x + W, y, x + W, y + W, WHITE);
	}
	if (self->walls[WALL_BOTTOM])
	{
		DrawLine(x, y + W, x + W, y + W, WHITE);
	}
	if (self->walls[WALL_LEFT])
	{
		DrawLine(x, y, x, y + W, WHITE);
	}
}

ChosenCell Cell_checkNeighbors(Cell* self, Cell* grid)
{
	vec(ChosenCell) neighbors;
	vec_init(&neighbors);
	Cell* top = getAt(grid, self->i, self->j - 1);
	Cell* right = getAt(grid, self->i + 1, self->j);
	Cell* bottom = getAt(grid, self->i, self->j + 1);
	Cell* left = getAt(grid, self->i - 1, self->j);
	if (top && !top->visited)
	{
		vec_push(&neighbors, ((ChosenCell){top, WALL_TOP}));
	}
	if (right && !right->visited)
	{
		vec_push(&neighbors, ((ChosenCell){right, WALL_RIGHT}));
	}
	if (bottom && !bottom->visited)
	{
		vec_push(&neighbors, ((ChosenCell){bottom, WALL_BOTTOM}));
	}
	if (left && !left->visited)
	{
		vec_push(&neighbors, ((ChosenCell){left, WALL_LEFT}));
	}

	if (neighbors.length > 0)
	{
		size_t r = randombytes_uniform(neighbors.length);
		ChosenCell chosen = neighbors.ptr[r];
		vec_deinit(&neighbors);
		return chosen;
	}
	else
	{
		return (ChosenCell){NULL, 0};
	}
}

void removeWalls(Cell* current, ChosenCell next)
{
	switch (next.side)
	{
		case WALL_TOP:
			current->walls[WALL_TOP] = false;
			next.cell->walls[WALL_BOTTOM] = false;
			break;
		case WALL_RIGHT:
			current->walls[WALL_RIGHT] = false;
			next.cell->walls[WALL_LEFT] = false;
			break;
		case WALL_BOTTOM:
			current->walls[WALL_BOTTOM] = false;
			next.cell->walls[WALL_TOP] = false;
			break;
		case WALL_LEFT:
			current->walls[WALL_LEFT] = false;
			next.cell->walls[WALL_RIGHT] = false;
			break;
		default:
			assert(false && "impossible");
	}
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(WIDTH, HEIGHT, "Maze Generator");

	Cell grid[ROWS * COLS];

	for (size_t j = 0; j < ROWS; ++j)
	{
		for (size_t i = 0; i < COLS; ++i)
		{
			Cell c;
			Cell_init(&c, i, j);
			grid[i + j * COLS] = c;
		}
	}

	Cell* current = &grid[0];
	vec(Cell*) stack;
	vec_init(&stack);

	double timer = 0;

	while (!WindowShouldClose())
	{
		if (timer > 0.2)
		{
			current->visited = true;
			ChosenCell next = Cell_checkNeighbors(current, grid);
			current->highlighted = false;
			if (next.cell)
			{
				next.cell->visited = true;
				next.cell->highlighted = true;
				removeWalls(current, next);
				vec_push(&stack, current);
				current = next.cell;
			}
			else if (stack.length > 0)
			{
				current = vec_pop(&stack);
				current->highlighted = true;
			}
			timer = 0;
		}
		timer += GetFrameTime();
		BeginDrawing();
		ClearBackground(CLITERAL(Color){51, 51, 51, 255});

		for (size_t i = 0; i < ROWS * COLS; ++i)
		{
			Cell_show(&grid[i]);
		}
		EndDrawing();
	}

	vec_deinit(&stack);
	CloseWindow();
}
