#include <common/vec.h>
#include <raylib.h>
#include <sodium.h>

#include "boid.h"

static Boids_t flock;

static void setup()
{
	vec_init(&flock);
	for (size_t i = 0; i < 100; ++i)
	{
		vec_push(&flock, Boid_new());
	}
}

static void draw()
{
	ClearBackground((Color){51, 51, 51});
	for (size_t i = 0; i < flock.length; ++i)
	{
		Boid_edges(&flock.ptr[i]);
		Boid_update(&flock.ptr[i]);
		Boid_show(&flock.ptr[i]);
		Boid_flock(&flock.ptr[i], &flock);
	}
}

int main()
{
	InitWindow(640, 360, "Boids");
	SetTargetFPS(60);

	setup();

	while (!WindowShouldClose())
	{
		BeginDrawing();
		draw();
		EndDrawing();
	}

	CloseWindow();
}
