#pragma once

#include <common/vec.h>
#include <raylib.h>

typedef struct Boid_t
{
	Vector2 position;
	Vector2 velocity;
	Vector2 acceleration;
} Boid_t;

typedef vec(Boid_t) Boids_t;

Boid_t Boid_new(void);
void Boid_show(Boid_t* boid);
void Boid_update(Boid_t* boid);
void Boid_edges(Boid_t* boid);
Vector2 Boid_align(Boid_t* boid, Boids_t* boids);
void Boid_flock(Boid_t* boid, Boids_t* boids);
