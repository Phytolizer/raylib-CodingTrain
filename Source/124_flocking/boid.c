#include "boid.h"

#include <common/vector2.h>
#include <sodium.h>

#include "common/random.h"

#define MAX_FORCE (double)0.2
#define MAX_SPEED (double)4

Boid_t Boid_new(void)
{
	Boid_t boid = (Boid_t){
	        .position
	        = (Vector2){random_between(0, GetScreenWidth()), random_between(0, GetScreenHeight())},
	        .velocity = Vector2_random(),
	        .acceleration = (Vector2){0, 0},
	};
	boid.velocity = Vector2_setMagnitude(boid.velocity, random_between(2, 4));
	return boid;
}

void Boid_show(Boid_t* boid)
{
	DrawCircleV(boid->position, 4, WHITE);
}

void Boid_update(Boid_t* boid)
{
	boid->velocity = Vector2_add(boid->velocity, boid->acceleration);
	boid->position = Vector2_add(boid->position, boid->velocity);
}

void Boid_edges(Boid_t* boid)
{
	if (boid->position.x >= GetScreenWidth())
	{
		boid->position.x = 0;
	}
	if (boid->position.y >= GetScreenHeight())
	{
		boid->position.y = 0;
	}
	if (boid->position.x < 0)
	{
		boid->position.x = GetScreenWidth() - 1;
	}
	if (boid->position.y < 0)
	{
		boid->position.y = GetScreenHeight() - 1;
	}
}

Vector2 Boid_align(Boid_t* boid, Boids_t* boids)
{
	double perceptionRadius = 50;
	Vector2 steering = {0};
	size_t total = 0;
	for (size_t i = 0; i < boids->length; ++i)
	{
		Boid_t* other = &boids->ptr[i];
		if (other == boid)
		{
			continue;
		}

		double d = Vector2_dist(boid->position, other->position);
		if (d < perceptionRadius)
		{
			steering = Vector2_add(steering, other->velocity);
			++total;
		}
	}
	if (total > 0)
	{
		steering = Vector2_scale(steering, 1 / (double)total);
		steering = Vector2_setMagnitude(steering, MAX_SPEED);
		steering = Vector2_sub(steering, boid->velocity);
		if (Vector2_magnitude(steering) > MAX_FORCE)
		{
			steering = Vector2_setMagnitude(steering, MAX_FORCE);
		}
	}
	return steering;
}

void Boid_flock(Boid_t* boid, Boids_t* boids)
{
	Vector2 alignment = Boid_align(boid, boids);
	boid->acceleration = alignment;
}
