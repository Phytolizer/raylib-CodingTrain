#include <stdlib.h>

#include <common/vec.h>
#include <raylib.h>
#include <raymath.h>

typedef struct Box
{
	Vector3 pos;
	double r;
} Box;

typedef vec(Box) BoxVec;

void Box_init(Box* self, double x, double y, double z, double r)
{
	self->pos.x = x;
	self->pos.y = y;
	self->pos.z = z;
	self->r = r;
}

BoxVec Box_generate(Box* self)
{
	BoxVec boxes;
	vec_init(&boxes);
	for (int x = -1; x < 2; ++x)
	{
		for (int y = -1; y < 2; ++y)
		{
			for (int z = -1; z < 2; ++z)
			{
				double new_r = self->r / 3;
				Box b;
				Box_init(&b, x * new_r, y * new_r, z * new_r, new_r);
				vec_push(&boxes, b);
			}
		}
	}
	return boxes;
}

void Box_show(Box* self)
{
	DrawCubeWires(self->pos, self->r, self->r, self->r, WHITE);
}

int main()
{
	InitWindow(400, 400, "Menger Sponge");

	double a = 0;
	Camera camera = {0};
	camera.position = CLITERAL(Vector3){200, 200, 0};
	camera.target = CLITERAL(Vector3){0, 0, 0};
	camera.up = CLITERAL(Vector3){0, 1, 0};
	camera.fovy = 45.0;
	camera.projection = CAMERA_PERSPECTIVE;
	SetCameraMode(camera, CAMERA_ORBITAL);

	Box b;
	Box_init(&b, 0, 0, 0, 200);

	while (!WindowShouldClose())
	{
		UpdateCamera(&camera);
		BeginDrawing();
		ClearBackground(CLITERAL(Color){51, 51, 51, 255});
		BeginMode3D(camera);

		Box_show(&b);

		EndMode3D();
		EndDrawing();
	}

	CloseWindow();
	return EXIT_SUCCESS;
}
