#include <stdint.h>

#include <common/vec.h>
#include <raylib.h>

#include "common/random.h"
#include "raymath.h"

#define SCREEN_WIDTH 200
#define SCREEN_HEIGHT 200

#define DA 1.0
#define DB 0.5
#define FEED 0.055
#define K 0.062

typedef struct ChemicalMix
{
	double a;
	double b;
} ChemicalMix;

typedef vec(ChemicalMix) ChemicalMixVec;

double laplaceA(ChemicalMixVec grid, int64_t x, int64_t y)
{
	double sumA = 0;

	sumA += grid.ptr[x + y * SCREEN_WIDTH].a * -1;
	if (x > 0)
	{
		sumA += grid.ptr[(x - 1) + y * SCREEN_WIDTH].a * 0.2;
	}
	if (x < SCREEN_WIDTH - 1)
	{
		sumA += grid.ptr[(x + 1) + y * SCREEN_WIDTH].a * 0.2;
	}
	if (y > 0)
	{
		sumA += grid.ptr[x + (y - 1) * SCREEN_WIDTH].a * 0.2;
	}
	if (y < SCREEN_HEIGHT - 1)
	{
		sumA += grid.ptr[x + (y + 1) * SCREEN_WIDTH].a * 0.2;
	}
	if (x > 0 && y > 0)
	{
		sumA += grid.ptr[(x - 1) + (y - 1) * SCREEN_WIDTH].a * 0.05;
	}
	if (x < SCREEN_WIDTH - 1 && y > 0)
	{
		sumA += grid.ptr[(x + 1) + (y - 1) * SCREEN_WIDTH].a * 0.05;
	}
	if (x > 0 && y < SCREEN_HEIGHT - 1)
	{
		sumA += grid.ptr[(x - 1) + (y + 1) * SCREEN_WIDTH].a * 0.05;
	}
	if (x < SCREEN_WIDTH - 1 && y < SCREEN_HEIGHT - 1)
	{
		sumA += grid.ptr[(x + 1) + (y + 1) * SCREEN_WIDTH].a * 0.05;
	}
	return sumA;
}

double laplaceB(ChemicalMixVec grid, int64_t x, int64_t y)
{
	double sumB = 0;

	sumB += grid.ptr[x + y * SCREEN_WIDTH].b * -1;
	if (x > 0)
	{
		sumB += grid.ptr[(x - 1) + y * SCREEN_WIDTH].b * 0.2;
	}
	if (x < SCREEN_WIDTH - 1)
	{
		sumB += grid.ptr[(x + 1) + y * SCREEN_WIDTH].b * 0.2;
	}
	if (y > 0)
	{
		sumB += grid.ptr[x + (y - 1) * SCREEN_WIDTH].b * 0.2;
	}
	if (y < SCREEN_HEIGHT - 1)
	{
		sumB += grid.ptr[x + (y + 1) * SCREEN_WIDTH].b * 0.2;
	}
	if (x > 0 && y > 0)
	{
		sumB += grid.ptr[(x - 1) + (y - 1) * SCREEN_WIDTH].b * 0.05;
	}
	if (x < SCREEN_WIDTH - 1 && y > 0)
	{
		sumB += grid.ptr[(x + 1) + (y - 1) * SCREEN_WIDTH].b * 0.05;
	}
	if (x > 0 && y < SCREEN_HEIGHT - 1)
	{
		sumB += grid.ptr[(x - 1) + (y + 1) * SCREEN_WIDTH].b * 0.05;
	}
	if (x < SCREEN_WIDTH - 1 && y < SCREEN_HEIGHT - 1)
	{
		sumB += grid.ptr[(x + 1) + (y + 1) * SCREEN_WIDTH].b * 0.05;
	}
	return sumB;
}

int main()
{
	InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Reaction-Diffusion");

	ChemicalMixVec grid;
	ChemicalMixVec next;
	vec_init(&grid);
	vec_init(&next);
	vec_reserve(&grid, SCREEN_WIDTH * SCREEN_HEIGHT);
	vec_reserve(&next, SCREEN_WIDTH * SCREEN_HEIGHT);
	for (int64_t x = 0; x < SCREEN_WIDTH; ++x)
	{
		for (int64_t y = 0; y < SCREEN_HEIGHT; ++y)
		{
			grid.ptr[x + y * SCREEN_WIDTH] = (ChemicalMix){1, 0};
			next.ptr[x + y * SCREEN_WIDTH] = (ChemicalMix){0, 0};
		}
	}

	double timer = 0;

	while (!WindowShouldClose())
	{
		if (timer > 1.0 / 60.0)
		{
			for (int64_t x = 0; x < SCREEN_WIDTH; ++x)
			{
				for (int64_t y = 0; y < SCREEN_HEIGHT; ++y)
				{
					double a = grid.ptr[x + y * SCREEN_WIDTH].a;
					double b = grid.ptr[x + y * SCREEN_WIDTH].b;
					next.ptr[x + y * SCREEN_WIDTH].a = a
					        + DA * laplaceA(grid, x, y) - a * b * b
					        + FEED * (1 - a);
					next.ptr[x + y * SCREEN_WIDTH].b = b
					        + DB * laplaceB(grid, x, y) + a * b * b
					        - (K + FEED) * b;
					next.ptr[x + y * SCREEN_WIDTH].a
					        = Clamp(next.ptr[x + y * SCREEN_WIDTH].a, 0, 1);
					next.ptr[x + y * SCREEN_WIDTH].b
					        = Clamp(next.ptr[x + y * SCREEN_WIDTH].b, 0, 1);
				}
			}

			for (int64_t x = 0; x < SCREEN_WIDTH; ++x)
			{
				for (int64_t y = 0; y < SCREEN_HEIGHT; ++y)
				{
					grid.ptr[x + y * SCREEN_WIDTH].a
					        = next.ptr[x + y * SCREEN_WIDTH].a;
					grid.ptr[x + y * SCREEN_WIDTH].b
					        = next.ptr[x + y * SCREEN_WIDTH].b;
				}
			}
			timer = 0;
		}
		timer += GetFrameTime();
		if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			Vector2 mousePos = GetMousePosition();
			for (int64_t x = mousePos.x - 5; x < mousePos.x + 5; ++x)
			{
				for (int64_t y = mousePos.y - 5; y < mousePos.y + 5; ++y)
				{
					if (x >= 0 && x < SCREEN_WIDTH && y >= 0
					    && y < SCREEN_HEIGHT)
					{
						grid.ptr[x + y * SCREEN_WIDTH].b = 1;
					}
				}
			}
		}
		BeginDrawing();
		ClearBackground((Color){51, 51, 51, 255});
		for (int64_t x = 0; x < SCREEN_WIDTH; ++x)
		{
			for (int64_t y = 0; y < SCREEN_HEIGHT; ++y)
			{
				Color c = {
				        grid.ptr[x + y * SCREEN_WIDTH].a * 255,
				        0,
				        grid.ptr[x + y * SCREEN_WIDTH].b * 255,
				        255,
				};
				DrawPixel(x, y, c);
			}
		}
		EndDrawing();
	}

	vec_deinit(&grid);
	CloseWindow();
}
