#include <common/vec.h>
#include <common/vector2.h>
#include <raylib.h>
#include <sodium.h>
#include <sodium/randombytes.h>

#ifndef M_PI
#	define M_PI 3.14159265358979
#endif

#define R1 200
#define R2 200
#define M1 20
#define M2 20

static Camera2D camera = {0};
static RenderTexture2D image;
static double a1 = M_PI / 2;
static double a2 = M_PI / 2;
static double a1_v = 0;
static double a2_v = 0;
static int width = 900;
static int height = 600;
static double px2 = -1;
static double py2 = -1;
#define G 1

static void setup()
{
	InitWindow(width, height, "Double Pendulum");
	SetTargetFPS(60);
	image = LoadRenderTexture(width, height);
	BeginTextureMode(image);
	ClearBackground(WHITE);
	EndTextureMode();
	camera.offset = (Vector2){
	        .x = (double)width / 2,
	        .y = (double)height / 2,
	};
	camera.rotation = 0.0;
	camera.zoom = 1.0;
	// a1 = (double)randombytes_random() / (double)UINT32_MAX;
	a2 = (double)randombytes_random() / (double)UINT32_MAX;
}

static void draw()
{
	float num1 = -G * (2 * M1 + M2) * sin(a1);
	float num2 = -M2 * G * sin(a1 - 2 * a2);
	float num3 = -2 * sin(a1 - a2) * M2;
	float num4 = a2_v * a2_v * R2 + a1_v * a1_v * R1 * cos(a1 - a2);
	float den = R1 * (2 * M1 + M2 - M2 * cos(2 * a1 - 2 * a2));
	float a1_a = (num1 + num2 + num3 * num4) / den;

	num1 = 2 * sin(a1 - a2)
	        * (a1_v * a1_v * R1 * (M1 + M2) + G * (M1 + M2) * cos(a1)
	           + a2_v * a2_v * R2 * M2 * cos(a1 - a2));
	den = R2 * (2 * M1 + M2 - M2 * cos(2 * a1 - 2 * a2));
	float a2_a = num1 / den;

	BeginDrawing();
	DrawTextureRec(
	        image.texture,
	        (Rectangle){
	                0,
	                0,
	                (float)image.texture.width,
	                -(float)image.texture.height},
	        (Vector2){0, 0},
	        WHITE);

	BeginMode2D(camera);
	camera.offset = (Vector2){450, 50};

	double x1 = R1 * sin(a1);
	double y1 = R1 * cos(a1);

	double x2 = x1 + R2 * sin(a2);
	double y2 = y1 + R2 * cos(a2);

	DrawLine(0, 0, x1, y1, BLACK);
	DrawCircle(x1, y1, M1, BLACK);

	DrawLine(x1, y1, x2, y2, BLACK);
	DrawCircle(x2, y2, M2, BLACK);

	EndMode2D();
	EndDrawing();

	BeginTextureMode(image);
	BeginMode2D(camera);
	if (px2 != -1)
	{
		DrawLineEx(
		        (Vector2){px2, py2},
		        (Vector2){x2, y2},
		        2,
		        (Color){0, 0, 0, 100});
	}
	EndMode2D();
	EndTextureMode();

	a1_v += a1_a;
	a2_v += a2_a;
	a1 += a1_v;
	a2 += a2_v;

	// a1_v *= 0.999;
	// a2_v *= 0.999;

	px2 = x2;
	py2 = y2;
}

int main()
{
	setup();

	while (!WindowShouldClose())
	{
		draw();
	}

	CloseWindow();
}
