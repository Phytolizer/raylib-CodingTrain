#include <stdio.h>
#include <string.h>

#include <cglm/cglm.h>
#include <common/math.h>
#include <common/matrix.h>
#include <raylib.h>
#include <sds/sds.h>

typedef struct Rule
{
	const char* a;
	const char* b;
} Rule;

const Rule RULES[] = {
        {.a = "F", .b = "FF+[+F-F-F]-[-F+F+F]"},
};

#define RULES_SIZE (sizeof(RULES) / sizeof(Rule))

#ifndef M_PI
#	define M_PI 3.141592653589793238
#endif

sds generate(sds sentence)
{
	sds nextSentence = sdsempty();
	for (size_t i = 0; i < sdslen(sentence); ++i)
	{
		char current = sentence[i];
		bool found = false;
		for (size_t j = 0; j < RULES_SIZE; ++j)
		{
			if (RULES[j].a[0] == current)
			{
				found = true;
				nextSentence = sdscat(nextSentence, RULES[j].b);
				break;
			}
		}
		if (!found)
		{
			nextSentence = sdscatlen(nextSentence, &current, 1);
		}
	}
	sdsfree(sentence);
	return nextSentence;
}

void turtle(sds sentence, double len)
{
	vec(mat4*) stack;
	vec_init(&stack);
	mat4* id = malloc(sizeof(mat4));
	glm_mat4_identity(*id);
	vec3 initialTranslation = {
	        (int)(GetScreenWidth() / 2),
	        GetScreenHeight(),
	        0,
	};
	glm_translate(*id, initialTranslation);
	vec_push(&stack, id);
	double rotationAngle = Radians(25);
	for (size_t i = 0; i < sdslen(sentence); ++i)
	{
		char current = sentence[i];
		switch (current)
		{
			case 'F':
				{
					vec4 origin = {0, 0, 0, 1};
					glm_mat4_mulv(**vec_last(&stack), origin, origin);
					vec3 translation = {0, -len, 0};
					glm_translate(**vec_last(&stack), translation);
					vec4 after = {0, 0, 0, 1};
					glm_mat4_mulv(**vec_last(&stack), after, after);
					DrawLine(
					        origin[0],
					        origin[1],
					        after[0],
					        after[1],
					        (Color){255, 255, 255, 100});
					break;
				}
			case '+':
				{
					vec3 axis = {0, 0, -1};
					glm_rotate(**vec_last(&stack), rotationAngle, axis);
					break;
				}
			case '-':
				{
					vec3 axis = {0, 0, -1};
					glm_rotate(**vec_last(&stack), -rotationAngle, axis);
					break;
				}
			case '[':
				{
					mat4* id = malloc(sizeof(mat4));
					glm_mat4_copy(**vec_last(&stack), *id);
					vec_push(&stack, id);
					break;
				}
			case ']':
				{
					mat4* back = vec_pop(&stack);
					free(back);
					break;
				}
		}
	}
	for (size_t i = 0; i < stack.length; ++i)
	{
		free(stack.ptr[i]);
	}
	vec_deinit(&stack);
}

int main()
{
	const char* axiom = "F";
	sds sentence = sdsnew(axiom);

	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(400, 400, "L-System Fractal Trees");
	double len = 100;

	while (!WindowShouldClose())
	{
		if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			sentence = generate(sentence);
			len *= 0.5;
		}
		BeginDrawing();
		ClearBackground((Color){51, 51, 51, 255});
		turtle(sentence, len);
		EndDrawing();
	}

	CloseWindow();
}
