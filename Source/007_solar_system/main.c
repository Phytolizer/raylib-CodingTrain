#include <math.h>

#include <cglm/cglm.h>
#include <cglm/mat3.h>
#include <common/random.h>
#include <common/vec.h>
#include <raylib.h>

#ifndef M_PI
#	define M_PI 3.14159265358979323846
#endif

typedef struct Planet
{
	double radius;
	double angle;
	double d;
	vec(struct Planet) children;
	double orbitSpeed;
} Planet;

typedef vec(Planet) Planets;

void Planet_init(Planet* self, double r, double d, double o)
{
	self->radius = r;
	self->angle = random_between(0, 2 * M_PI);
	self->d = d;
	self->orbitSpeed = o;
	vec_init(&self->children);
}

void Planet_deinit(Planet* self)
{
	for (size_t i = 0; i < self->children.length; ++i)
	{
		Planet_deinit(&self->children.ptr[i]);
	}
	vec_deinit(&self->children);
}

void Planet_spawnMoons(Planet* self, size_t total, size_t level)
{
	vec_resize(&self->children, total);
	for (size_t i = 0; i < self->children.length; ++i)
	{
		Planet_init(
		        &self->children.ptr[i],
		        self->radius / 2,
		        random_between(self->radius * 2, self->radius * 3),
		        random_between(0.02, 0.1));
		if (level < 2)
		{
			Planet_spawnMoons(
			        &self->children.ptr[i],
			        random_between(0, 4),
			        level + 1);
		}
	}
}

void Planet_orbit(Planet* self)
{
	self->angle += self->orbitSpeed;
	for (size_t i = 0; i < self->children.length; ++i)
	{
		Planet_orbit(&self->children.ptr[i]);
	}
}

void Planet_show(Planet* self, mat3 mat)
{
	glm_rotate2d(mat, self->angle);
	vec2 translation = {self->d, 0};
	glm_translate2d(mat, translation);
	vec3 pos = {0, 0, 1};
	glm_mat3_mulv(mat, pos, pos);
	DrawEllipse(
	        pos[0],
	        pos[1],
	        self->radius,
	        self->radius,
	        CLITERAL(Color){
	                255,
	                255,
	                255,
	                100,
	        });

	for (size_t i = 0; i < self->children.length; ++i)
	{
		mat3 mat2;
		glm_mat3_copy(mat, mat2);
		Planet_show(&self->children.ptr[i], mat2);
	}
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(600, 600, "Solar System");
	Planet sun;
	Planet_init(&sun, 50, 0, 0);
	Planet_spawnMoons(&sun, 5, 0);
	double timer = 0;

	while (!WindowShouldClose())
	{
		if (timer >= 1.0 / 60.0)
		{
			Planet_orbit(&sun);
			timer = 0;
		}
		timer += GetFrameTime();
		BeginDrawing();
		ClearBackground(BLACK);
		mat3 mat;
		glm_mat3_identity(mat);
		vec2 initialTranslation = {
		        (double)GetScreenWidth() / 2,
		        (double)GetScreenHeight() / 2,
		};
		glm_translate2d(mat, initialTranslation);
		Planet_show(&sun, mat);
		EndDrawing();
	}

	Planet_deinit(&sun);
	CloseWindow();
}
