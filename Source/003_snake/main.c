#include <stdio.h>

#include <common/math.h>
#include <common/vec.h>
#include <common/vector2.h>
#include <raylib.h>
#include <raymath.h>
#include <sodium.h>

#define SCALE 20

typedef vec(Vector2) SnakeTail;

typedef struct
{
	double x;
	double y;
	double xspeed;
	double yspeed;
	int total;
	SnakeTail tail;
} Snake;

void Snake_init(Snake* self)
{
	self->x = 0;
	self->y = 0;
	self->xspeed = 1;
	self->yspeed = 0;
	self->total = 0;
	vec_init(&self->tail);
}

void Snake_deinit(Snake* self)
{
	vec_deinit(&self->tail);
}

void Snake_dir(Snake* self, double xspeed, double yspeed)
{
	self->xspeed = xspeed;
	self->yspeed = yspeed;
}

void Snake_update(Snake* self)
{
	for (int i = 0; i < self->total - 1; ++i)
	{
		self->tail.ptr[i] = self->tail.ptr[i + 1];
	}
	if (self->total > 0)
	{
		self->tail.ptr[self->total - 1] = CLITERAL(Vector2){self->x, self->y};
	}

	self->x += self->xspeed * SCALE;
	self->y += self->yspeed * SCALE;

	self->x = Clamp(self->x, 0, GetScreenWidth() - SCALE);
	self->y = Clamp(self->y, 0, GetScreenHeight() - SCALE);
}

void Snake_show(Snake* self)
{
	for (int i = 0; i < self->total; ++i)
	{
		DrawRectangle(
		        self->tail.ptr[i].x,
		        self->tail.ptr[i].y,
		        SCALE,
		        SCALE,
		        WHITE);
	}
	DrawRectangle(self->x, self->y, SCALE, SCALE, WHITE);
}

bool Snake_eat(Snake* self, Vector2 food)
{
	double d = Vector2_magnitude(Vector2_sub(
	        CLITERAL(Vector2){self->x / SCALE, self->y / SCALE},
	        food));
	if (d < 1)
	{
		++self->total;
		vec_push(&self->tail, (CLITERAL(Vector2){self->x, self->y}));
		return true;
	}
	return false;
}

void Snake_death(Snake* self)
{
	for (int i = 0; i < self->total; ++i)
	{
		double d = Vector2_magnitude(Vector2_sub(
		        CLITERAL(Vector2){self->x, self->y},
		        self->tail.ptr[i]));
		if (d < 1)
		{
			self->total = 0;
			vec_resize(&self->tail, 0);
		}
	}
}

Vector2 pickLocation(void)
{
	return CLITERAL(Vector2){
	        randombytes_uniform(GetScreenWidth() / SCALE),
	        randombytes_uniform(GetScreenHeight() / SCALE),
	};
}

int main()
{
	InitWindow(600, 600, "Snake");

	Snake s;
	Snake_init(&s);
	Vector2 food = pickLocation();

	double timer = 0;

	while (!WindowShouldClose())
	{
		timer += GetFrameTime();
		if (timer >= 0.1)
		{
			Snake_update(&s);
			Snake_death(&s);
			if (Snake_eat(&s, food))
			{
				food = pickLocation();
			}
			timer = 0;
		}
		if (IsKeyPressed(KEY_UP))
		{
			Snake_dir(&s, 0, -1);
		}
		else if (IsKeyPressed(KEY_DOWN))
		{
			Snake_dir(&s, 0, 1);
		}
		else if (IsKeyPressed(KEY_LEFT))
		{
			Snake_dir(&s, -1, 0);
		}
		else if (IsKeyPressed(KEY_RIGHT))
		{
			Snake_dir(&s, 1, 0);
		}
		BeginDrawing();
		ClearBackground(CLITERAL(Color){51, 51, 51, 255});
		Snake_show(&s);
		DrawRectangle(
		        food.x * SCALE,
		        food.y * SCALE,
		        SCALE,
		        SCALE,
		        CLITERAL(Color){255, 0, 100, 255});
		EndDrawing();
	}

	Snake_deinit(&s);
	CloseWindow();
}
