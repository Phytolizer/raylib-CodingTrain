#include "common/random.h"

#include <sodium/randombytes.h>

double random_between(double min, double max)
{
	uint32_t r = randombytes_random();
	double r2 = (double)r / UINT32_MAX;
	return r2 * (max - min) + min;
}
