#include "common/vector2.h"

#include <cglm/cglm.h>
#include <cglm/mat3.h>
#include <common/random.h>
#include <sodium/randombytes.h>

Vector2 Vector2_add(Vector2 a, Vector2 b)
{
	return (Vector2){a.x + b.x, a.y + b.y};
}

Vector2 Vector2_sub(Vector2 a, Vector2 b)
{
	return (Vector2){b.x - a.x, b.y - a.y};
}

Vector2 Vector2_scale(Vector2 v, double s)
{
	return (Vector2){v.x * s, v.y * s};
}

double Vector2_magnitude(Vector2 v)
{
	return sqrt(v.x * v.x + v.y * v.y);
}

Vector2 Vector2_setMagnitude(Vector2 v, double magnitude)
{
	double d = Vector2_magnitude(v);
	if (d == 0)
	{
		return v;
	}
	return (Vector2){
	        .x = v.x / d * magnitude,
	        .y = v.y / d * magnitude,
	};
}

Vector2 Vector2_normalize(Vector2 v)
{
	double d = Vector2_magnitude(v);
	if (d == 0)
	{
		return v;
	}
	return (Vector2){
	        .x = v.x / d,
	        .y = v.y / d,
	};
}

double Vector2_dist(Vector2 a, Vector2 b)
{
	double dx = a.x - b.x;
	double dy = a.y - b.y;
	return sqrt(dx * dx + dy * dy);
}

Vector2 Vector2_random(void)
{
	double x = random_between(-1, 1);
	double y = random_between(-1, 1);

	return Vector2_normalize((Vector2){
	        .x = x,
	        .y = y,
	});
}

Vector2 Vector2_rotate(Vector2 v, double angle)
{
	vec3 orig = {v.x, v.y, 1};
	mat3 rot;
	glm_mat3_identity(rot);
	glm_rotate2d(rot, angle);
	glm_mat3_mulv(rot, orig, orig);
	return (Vector2){orig[0], orig[1]};
}
