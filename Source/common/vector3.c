#include "common/vector3.h"

#include <math.h>

#include <common/random.h>

Vector3 Vector3_add(Vector3 a, Vector3 b)
{
	return (Vector3){
	        .x = a.x + b.x,
	        .y = a.y + b.y,
	        .z = a.z + b.z,
	};
}

Vector3 Vector3_scale(Vector3 v, double scale)
{
	return (Vector3){
	        .x = v.x * scale,
	        .y = v.y * scale,
	        .z = v.z * scale,
	};
}

Vector3 Vector3_cross(Vector3 a, Vector3 b)
{
	return (Vector3){
	        .x = a.y * b.z - a.z * b.y,
	        .y = -a.x * b.z + a.z * b.x,
	        .z = a.x * b.y - a.y * b.x,
	};
}

double Vector3_dot(Vector3 a, Vector3 b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

double Vector3_magnitude(Vector3 v)
{
	return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

Vector3 Vector3_normalize(Vector3 v)
{
	double m = Vector3_magnitude(v);
	return (Vector3){
	        .x = v.x / m,
	        .y = v.y / m,
	        .z = v.z / m,
	};
}

Vector3 Vector3_sub(Vector3 a, Vector3 b)
{
	return (Vector3){
	        .x = a.x - b.x,
	        .y = a.y - b.y,
	        .z = a.z - b.z,
	};
}

Vector3 Vector3_random(void)
{
	double x = random_between(0, 1);
	double y = random_between(0, 1);
	double z = random_between(0, 1);
	return Vector3_normalize(CLITERAL(Vector3){x, y, z});
}
