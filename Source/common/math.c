#include "common/math.h"

#include <stdio.h>

#ifndef M_PI
#	define M_PI 3.141592653589793
#endif

double Radians(double degrees)
{
	return degrees * M_PI / 180.0;
}

double Map(
        double x,
        double lower,
        double upper,
        double newLower,
        double newUpper)
{
	double unmapped = (x - lower) / (upper - lower);
	return (newUpper - newLower) * unmapped + newLower;
}
