#include "common/matrix.h"

#include <cglm/mat4.h>

Vector3 matrixTimesVector(Matrix m, Vector3 v)
{
	Vector4 t = {
	        .x = v.x,
	        .y = v.y,
	        .z = v.z,
	        .w = 1,
	};
	Vector4 result = {
	        .x = m.m0 * t.x + m.m4 * t.y + m.m8 * t.z + m.m12 * t.w,
	        .y = m.m1 * t.x + m.m5 * t.y + m.m9 * t.z + m.m13 * t.w,
	        .z = m.m2 * t.x + m.m6 * t.y + m.m10 * t.z + m.m14 * t.w,
	        .w = m.m3 * t.x + m.m7 * t.y + m.m11 * t.z + m.m15 * t.w,
	};
	return (Vector3){
	        .x = result.x / result.w,
	        .y = result.y / result.w,
	        .z = result.z / result.w,
	};
}

Matrix ConvertGlmMatrixToRaylib(mat4 matrix)
{
	return (Matrix){
	        .m0 = matrix[0][0],
	        .m1 = matrix[0][1],
	        .m2 = matrix[0][2],
	        .m3 = matrix[0][3],
	        .m4 = matrix[1][0],
	        .m5 = matrix[1][1],
	        .m6 = matrix[1][2],
	        .m7 = matrix[1][3],
	        .m8 = matrix[2][0],
	        .m9 = matrix[2][1],
	        .m10 = matrix[2][2],
	        .m11 = matrix[2][3],
	        .m12 = matrix[3][0],
	        .m13 = matrix[3][1],
	        .m14 = matrix[3][2],
	        .m15 = matrix[3][3],
	};
}

void ConvertRaylibMatrixToGlm(Matrix matrix, mat4 dest)
{
	dest[0][0] = matrix.m0;
	dest[0][1] = matrix.m1;
	dest[0][2] = matrix.m2;
	dest[0][3] = matrix.m3;
	dest[1][0] = matrix.m4;
	dest[1][1] = matrix.m5;
	dest[1][2] = matrix.m6;
	dest[1][3] = matrix.m7;
	dest[2][0] = matrix.m8;
	dest[2][1] = matrix.m9;
	dest[2][2] = matrix.m10;
	dest[2][3] = matrix.m11;
	dest[3][0] = matrix.m12;
	dest[3][1] = matrix.m13;
	dest[3][2] = matrix.m14;
	dest[3][3] = matrix.m15;
}

void MatrixStack_init(MatrixStack* stack)
{
	vec_init(stack);
}

void MatrixStack_deinit(MatrixStack* stack)
{
	vec_deinit(stack);
}

void MatrixStack_push(MatrixStack* stack, Matrix m)
{
	vec_push(stack, m);
}

void MatrixStack_pushGlm(MatrixStack* stack, mat4 m)
{
	vec_push(stack, ConvertGlmMatrixToRaylib(m));
}

void MatrixStack_pushIdentity(MatrixStack* stack)
{
	mat4 m;
	glm_mat4_identity(m);
	vec_push(stack, ConvertGlmMatrixToRaylib(m));
}

Matrix* MatrixStack_top(MatrixStack* stack)
{
	return vec_last(stack);
}

Matrix MatrixStack_pop(MatrixStack* stack)
{
	return vec_pop(stack);
}
