#include <random>
#include <raylib.h>

struct Walker
{
	std::default_random_engine random_engine;
	float x = static_cast<float>(GetScreenWidth()) / 2;
	float y = static_cast<float>(GetScreenHeight()) / 2;

	void walk()
	{
		int motion = std::uniform_int_distribution<>(0, 3)(random_engine);
		if (motion < 2)
		{
			x += 2 * motion - 1;
		}
		else
		{
			y += 2 * (motion - 2) - 1;
		}
	}

	void display()
	{
		DrawCircle(x, y, 1, BLACK);
	}
};

int main()
{
	InitWindow(800, 600, "Random Walker");

	Walker w;

	auto target = LoadRenderTexture(GetScreenWidth(), GetScreenHeight());

	BeginDrawing();
	ClearBackground(WHITE);
	EndDrawing();

	while (!WindowShouldClose())
	{
		w.walk();
		BeginDrawing();
		ClearBackground(WHITE);
		BeginTextureMode(target);
		w.display();
		EndTextureMode();
		DrawTextureRec(
		        target.texture,
		        Rectangle{
		                0,
		                0,
		                static_cast<float>(GetScreenWidth()),
		                static_cast<float>(GetScreenHeight()),
		        },
		        Vector2{0, 0},
		        WHITE);
		EndDrawing();
	}

	CloseWindow();
}
