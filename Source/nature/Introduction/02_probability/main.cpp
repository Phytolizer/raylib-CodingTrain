#include <random>
#include <raylib.h>
#include <vector>

std::random_device gRandomDevice;
constexpr double kGravity = 0.1;

struct Particle
{
	double x;
	double y;
	double xspeed;
	double yspeed;

	Color color;

	Particle(double x, double y, double xspeed, double yspeed, Color color)
	        : x{x}, y{y}, xspeed{xspeed}, yspeed{yspeed}, color{color}
	{
	}
};

struct Particles
{
	std::vector<Particle> particles;

	void Add(std::default_random_engine& eng)
	{
		particles.emplace_back(
		        GetScreenWidth() / 2,
		        GetScreenHeight() / 2,
		        std::uniform_real_distribution<double>(-1, 1)(eng),
		        std::uniform_real_distribution<double>(-1, 1)(eng),
		        BLACK);
	}

	void Update(double dt)
	{
		for (auto& p : particles)
		{
			p.x += p.xspeed;
			p.y += p.yspeed;
			p.yspeed += kGravity * dt;
		}
	}

	void Show() const
	{
		for (const auto& p : particles)
		{
			DrawCircle(p.x, p.y, 10, p.color);
		}
	}
};

int main()
{
	std::default_random_engine eng{gRandomDevice()};
	Particles particles;
	InitWindow(600, 600, "Probability Demo");

	while (!WindowShouldClose())
	{
		BeginDrawing();
		ClearBackground(RAYWHITE);
		if (std::uniform_int_distribution<>(0, 100)(eng) < 1)
		{
			particles.Add(eng);
		}
		particles.Update(GetFrameTime());
		particles.Show();
		EndDrawing();
	}

	CloseWindow();
	return 0;
}
