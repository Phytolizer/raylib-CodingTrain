#include <math.h>
#include <stdint.h>

#include <cglm/cglm.h>
#include <raylib.h>

#define NK_IMPLEMENTATION
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#include <nuklear.h>

#ifndef M_PI
#	define M_PI 3.141592653589793
#endif

float dummy_text_width_calculation(
        nk_handle handle,
        float height,
        const char* text,
        int len)
{
	return 1;
}

void branch(mat3 mat, double len, float angle)
{
	if (len < 4)
	{
		return;
	}
	vec3 pos = {0, 0, 1};
	glm_mat3_mulv(mat, pos, pos);
	vec3 pos2 = {0, -len, 1};
	glm_mat3_mulv(mat, pos2, pos2);
	DrawLine(pos[0], pos[1], pos2[0], pos2[1], WHITE);

	mat3 next;
	glm_mat3_copy(mat, next);
	vec2 translation = {0, -len};
	glm_translate2d(next, translation);
	glm_rotate2d(next, angle);
	branch(next, len * 0.67, angle);
	glm_rotate2d(next, -angle * 2);
	branch(next, len * 0.67, angle);
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(400, 400, "Fractal Tree");
	struct nk_context ctx;
	struct nk_user_font font;
	font.userdata.ptr = NULL;
	font.height = 10;
	font.width = dummy_text_width_calculation;
	float sliderValue = 0;
	nk_init_default(&ctx, &font);

	while (!WindowShouldClose())
	{
		nk_input_begin(&ctx);
		{
			Vector2 mouse = GetMousePosition();
			nk_input_motion(&ctx, mouse.x, mouse.y);
			nk_input_button(
			        &ctx,
			        NK_BUTTON_LEFT,
			        mouse.x,
			        mouse.y,
			        IsMouseButtonDown(MOUSE_LEFT_BUTTON));
		}
		nk_input_end(&ctx);

		if (nk_begin(&ctx, "", (struct nk_rect){0, 0, 400, 70}, 0))
		{
			nk_layout_row_dynamic(&ctx, 30, 1);
			nk_slider_float(&ctx, 0, &sliderValue, 2 * M_PI, 0.1);
		}
		nk_end(&ctx);
		BeginDrawing();
		ClearBackground((Color){51, 51, 51, 255});
		mat3 mat;
		glm_mat3_identity(mat);
		vec2 initialTranslation = {200, 400};
		glm_translate2d(mat, initialTranslation);
		branch(mat, 100, sliderValue);

		const struct nk_command* cmd = NULL;
		Color color = {0};
		nk_foreach(cmd, &ctx)
		{
			switch (cmd->type)
			{
				case NK_COMMAND_NOP:
					break;
				case NK_COMMAND_RECT:
					{
						const struct nk_command_rect* rect
						        = (const struct nk_command_rect*)cmd;
						DrawRectangleLines(
						        rect->x,
						        rect->y,
						        rect->w,
						        rect->h,
						        (Color){rect->color.r,
						                rect->color.g,
						                rect->color.b,
						                rect->color.a});
					}
					break;
				case NK_COMMAND_CIRCLE:
					{
						const struct nk_command_circle* c
						        = (const struct nk_command_circle*)cmd;
						DrawCircleLines(
						        c->x + c->w / 2,
						        c->y + c->h / 2,
						        c->w,
						        (Color){c->color.r,
						                c->color.g,
						                c->color.b,
						                c->color.a});
					}
					break;
				case NK_COMMAND_RECT_FILLED:
					{
						const struct nk_command_rect_filled* r
						        = (const struct nk_command_rect_filled*)cmd;
						DrawRectangle(
						        r->x,
						        r->y,
						        r->w,
						        r->h,
						        (Color){r->color.r,
						                r->color.g,
						                r->color.b,
						                r->color.a});
					}
					break;
				case NK_COMMAND_CIRCLE_FILLED:
					{
						const struct nk_command_circle_filled* c
						        = (const struct nk_command_circle_filled*)cmd;
						DrawCircle(
						        c->x + c->w / 2,
						        c->y + c->h / 2,
						        c->w,
						        (Color){c->color.r,
						                c->color.g,
						                c->color.b,
						                c->color.a});
					}
					break;
				case NK_COMMAND_LINE:
					{
						const struct nk_command_line* line
						        = (const struct nk_command_line*)cmd;
						DrawLine(
						        line->begin.x,
						        line->begin.y,
						        line->end.x,
						        line->end.y,
						        (Color){line->color.r,
						                line->color.g,
						                line->color.b,
						                line->color.a});
					}
					break;
			}
		}
		EndDrawing();
		nk_clear(&ctx);
	}

	nk_free(&ctx);
	CloseWindow();
}
