#include <stddef.h>

#include <common/vec.h>
#include <raylib.h>

#define SCALE 20

int main()
{
	InitWindow(600, 600, "Perlin Noise Terrain");
	size_t w = 600;
	size_t h = 600;
	size_t cols = w / SCALE;
	size_t rows = h / SCALE;
	vec(Vector2) stripPoints;
	vec_init(&stripPoints);

	Camera camera = {0};
	camera.position = (Vector3){5, 5, 0};
	camera.target = (Vector3){0, 0, 0};
	camera.up = (Vector3){0, 1, 0};
	camera.fovy = 45;
	camera.projection = CAMERA_PERSPECTIVE;
	SetCameraMode(camera, CAMERA_ORBITAL);

	while (!WindowShouldClose())
	{
		vec_resize(&stripPoints, 0);
		BeginDrawing();
		BeginMode3D(camera);
		ClearBackground(BLACK);
		for (size_t y = 0; y < rows; ++y)
		{
			for (size_t x = 0; x < cols; ++x)
			{
				vec_push(&stripPoints, ((Vector2){x * SCALE, y * SCALE}));
				vec_push(&stripPoints, ((Vector2){x * SCALE, (y + 1) * SCALE}));
			}
		}
		DrawTriangleStrip(stripPoints.ptr, stripPoints.length, WHITE);
		EndMode3D();
		EndDrawing();
	}

	CloseWindow();
}
