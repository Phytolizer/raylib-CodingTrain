#include <math.h>

#include <cglm/cglm.h>
#include <common/random.h>
#include <common/vec.h>
#include <common/vector3.h>
#include <raylib.h>

#ifndef M_PI
#	define M_PI 3.14159265358979323846
#endif

typedef struct Planet
{
	double radius;
	double angle;
	double d;
	vec(struct Planet) children;
	double orbitSpeed;
	Vector3 v;
} Planet;

typedef vec(Planet) Planets;

void Planet_init(Planet* self, double r, double d, double o)
{
	self->radius = r;
	self->angle = random_between(0, 2 * M_PI);
	self->d = d;
	self->orbitSpeed = o;
	vec_init(&self->children);
	self->v = Vector3_scale(Vector3_random(), d);
}

void Planet_deinit(Planet* self)
{
	for (size_t i = 0; i < self->children.length; ++i)
	{
		Planet_deinit(&self->children.ptr[i]);
	}
	vec_deinit(&self->children);
}

void Planet_spawnMoons(Planet* self, size_t total, size_t level)
{
	vec_resize(&self->children, total);
	for (size_t i = 0; i < self->children.length; ++i)
	{
		double r = self->radius / (level * 2);
		Planet_init(
		        &self->children.ptr[i],
		        r,
		        random_between(self->radius + r, (self->radius + r) * 2),
		        random_between(-0.1, 0.1));
		if (level < 2)
		{
			Planet_spawnMoons(
			        &self->children.ptr[i],
			        random_between(0, 4),
			        level + 1);
		}
	}
}

void Planet_orbit(Planet* self)
{
	self->angle += self->orbitSpeed;
	for (size_t i = 0; i < self->children.length; ++i)
	{
		Planet_orbit(&self->children.ptr[i]);
	}
}

void Planet_show(Planet* self, mat4 mat)
{
	// vec3 zaxis = {0, 0, -1};
	// glm_rotate(mat, self->angle, zaxis);
	vec3 translation = {self->v.x, self->v.y, self->v.z};
	glm_translate(mat, translation);
	vec4 pos = {0, 0, 0, 1};
	glm_mat4_mulv(mat, pos, pos);
	DrawSphere(CLITERAL(Vector3){pos[0], pos[1], pos[2]}, self->radius, WHITE);

	for (size_t i = 0; i < self->children.length; ++i)
	{
		mat4 mat2;
		glm_mat4_copy(mat, mat2);
		Planet_show(&self->children.ptr[i], mat2);
	}
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(600, 600, "Solar System");
	Planet sun;
	Planet_init(&sun, 50, 0, 0);
	Planet_spawnMoons(&sun, 5, 1);
	double timer = 0;
	Camera camera = {
	        .position = {5, 5, 5},
	        .target = {0, 0, 0},
	        .fovy = 45,
	        .up = {0, 1, 0},
	        .projection = CAMERA_PERSPECTIVE,
	};

	while (!WindowShouldClose())
	{
		if (timer > 1.0 / 60.0)
		{
			timer = 0;
			Planet_orbit(&sun);
		}
		timer += GetFrameTime();
		BeginDrawing();
		BeginMode3D(camera);
		ClearBackground(BLACK);
		mat4 mat;
		glm_mat4_identity(mat);
		vec3 initialTranslation = {
		        (double)GetScreenWidth() / 2,
		        (double)GetScreenHeight() / 2,
		        0,
		};
		glm_translate(mat, initialTranslation);
		Planet_show(&sun, mat);
		EndMode3D();
		EndDrawing();
	}

	CloseWindow();
}
