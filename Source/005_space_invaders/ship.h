#pragma once

typedef struct
{
	double x;
} Ship;

void Ship_init(Ship* self);
void Ship_show(Ship* self);
void Ship_move(Ship* self, int dir);
