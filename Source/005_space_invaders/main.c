#include <stdio.h>

#include <common/random.h>
#include <common/vec.h>
#include <raylib.h>

#include "drop.h"
#include "flower.h"
#include "ship.h"

typedef vec(Drop*) Drops;

void HandleKeysPressed(Ship* s, Drops* drops)
{
	if (IsKeyPressed(KEY_SPACE))
	{
		Drop* drop = malloc(sizeof(Drop));
		Drop_init(drop, s->x, GetScreenHeight());
		vec_push(drops, drop);
	}
}

void HandleKeysDown(Ship* s)
{
	if (IsKeyDown(KEY_RIGHT))
	{
		Ship_move(s, 1);
	}
	if (IsKeyDown(KEY_LEFT))
	{
		Ship_move(s, -1);
	}
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(600, 400, "Space Invaders");
	double timer = 0;
	Ship s;
	Ship_init(&s);
	vec(Flower) flowers;
	vec_init(&flowers);
	Drops drops;
	vec_init(&drops);
	for (int i = 0; i < 6; ++i)
	{
		vec_push(&flowers, (Flower){0});
		Flower_init(vec_last(&flowers), i * 80 + 80, 60);
	}

	while (!WindowShouldClose())
	{
		timer += GetFrameTime();
		if (timer >= 1.0 / 60.0)
		{
			timer = 0;
			HandleKeysDown(&s);
			for (long i = drops.length - 1; i >= 0; --i)
			{
				Drop_move(drops.ptr[i]);
				for (long j = 0; j < flowers.length; ++j)
				{
					if (Drop_hits(drops.ptr[i], &flowers.ptr[j]))
					{
						Flower_grow(&flowers.ptr[j]);
						Drop_evaporate(drops.ptr[i]);
					}
				}
			}
			for (long i = drops.length - 1; i >= 0; --i)
			{
				if (drops.ptr[i]->toDelete)
				{
					free(drops.ptr[i]);
					vec_splice(&drops, i, 1);
				}
			}
		}
		HandleKeysPressed(&s, &drops);

		BeginDrawing();
		ClearBackground(CLITERAL(Color){51, 51, 51, 255});
		Ship_show(&s);
		for (long i = 0; i < drops.length; ++i)
		{
			Drop_show(drops.ptr[i]);
		}
		for (long i = 0; i < flowers.length; ++i)
		{
			Flower_show(&flowers.ptr[i]);
		}
		EndDrawing();
	}

	CloseWindow();
}
