#pragma once

typedef struct
{
	double x;
	double y;
	double r;
} Flower;

void Flower_init(Flower* f, double x, double y);
void Flower_show(Flower* f);
void Flower_grow(Flower* f);
