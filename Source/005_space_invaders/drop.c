#include "drop.h"

#include <common/vector2.h>
#include <raylib.h>

#undef BLUE
#define BLUE \
	CLITERAL(Color) \
	{ \
		50, 0, 255, 255 \
	}

void Drop_init(Drop* self, double x, double y)
{
	self->x = x;
	self->y = y;
	self->r = 4;
	self->toDelete = false;
}

void Drop_show(Drop* self)
{
	DrawEllipse(self->x, self->y, 4, 4, BLUE);
}

void Drop_move(Drop* self)
{
	self->y -= 1;
}

bool Drop_hits(Drop* self, Flower* f)
{
	return Vector2_dist(
	               CLITERAL(Vector2){self->x, self->y},
	               CLITERAL(Vector2){f->x, f->y})
	        < self->r + f->r;
}

void Drop_evaporate(Drop* self)
{
	self->toDelete = true;
}
