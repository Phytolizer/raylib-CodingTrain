#include "flower.h"

#include <raylib.h>

#ifdef PURPLE
#	undef PURPLE
#endif
#define PURPLE \
	CLITERAL(Color) \
	{ \
		255, 0, 200, 255 \
	}

void Flower_init(Flower* f, double x, double y)
{
	f->x = x;
	f->y = y;
	f->r = 30;
}

void Flower_show(Flower* f)
{
	DrawEllipse(f->x, f->y, f->r, f->r, PURPLE);
}

void Flower_grow(Flower* f)
{
	f->r += 2;
}
