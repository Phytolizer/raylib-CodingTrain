#include "ship.h"

#include <raylib.h>

void Ship_init(Ship* self)
{
	self->x = (double)GetScreenWidth() / 2;
}

void Ship_show(Ship* self)
{
	DrawRectangle(self->x - 10, GetScreenHeight() - 60, 20, 60, WHITE);
}

void Ship_move(Ship* self, int dir)
{
	self->x += dir * 5;
}
