#pragma once

#include <stdbool.h>

#include "flower.h"

typedef struct
{
	double x;
	double y;
	double r;
	bool toDelete;
} Drop;

void Drop_init(Drop* self, double x, double y);
void Drop_show(Drop* self);
void Drop_move(Drop* self);
bool Drop_hits(Drop* self, Flower* f);
void Drop_evaporate(Drop* self);
