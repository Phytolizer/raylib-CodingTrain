#include <raylib.h>

#include "common/vector2.h"

typedef struct Ship
{
	Vector2 pos;
	double r;
} Ship;

void Ship_init(Ship* s)
{
	s->pos = (Vector2){
	        (double)GetScreenWidth() / 2,
	        (double)GetScreenHeight() / 2,
	};
	s->r = 10;
}

void Ship_render(Ship* s, Camera2D* camera)
{
	camera->target = s->pos;
	DrawTriangle(
	        (Vector2){-s->r, s->r},
	        (Vector2){s->r, s->r},
	        (Vector2){0, s->r},
	        WHITE);
}

int main()
{
	InitWindow(800, 600, "Asteroids");

	Camera2D camera = {
	        .offset = {0, 0},
	        .target = {0, 0},
	        .rotation = 0,
	        .zoom = 1,
	};
	Ship s;

	Ship_init(&s);

	while (!WindowShouldClose())
	{
		BeginDrawing();
		ClearBackground(BLACK);

		BeginMode2D(camera);

		s.pos = GetMousePosition();

		Ship_render(&s, &camera);

		EndMode2D();

		EndDrawing();
	}

	CloseWindow();
	return 0;
}
