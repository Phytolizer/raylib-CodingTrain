#include <stdio.h>

#include <common/random.h>
#include <common/vec.h>
#include <common/vector2.h>
#include <raylib.h>

typedef struct
{
	Vector2 pos;
	double r;
	Color c;
} Cell;

typedef vec(Cell) Cells;

void Cell_init(Cell* self, Vector2 pos, double r, Color c)
{
	self->pos = pos;
	self->r = r;
	self->c = c;
}

void Cell_show(Cell* self)
{
	DrawEllipse(self->pos.x, self->pos.y, self->r, self->r, self->c);
}

void Cell_move(Cell* self)
{
	Vector2 vel = Vector2_random();
	self->pos = Vector2_add(self->pos, vel);
}

bool Cell_clicked(Cell* self, double x, double y)
{
	double d = Vector2_dist(self->pos, CLITERAL(Vector2){x, y});
	return d < self->r;
}

Cell Cell_mitosis(Cell* self)
{
	Cell newCell;
	Cell_init(&newCell, self->pos, self->r / 2, self->c);
	return newCell;
}

void HandleMouse(Cells* cells)
{
	Cells newCells;
	vec_init(&newCells);
	if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
	{
		for (size_t i = 0; i < cells->length; ++i)
		{
			if (Cell_clicked(&cells->ptr[i], GetMouseX(), GetMouseY()))
			{
				vec_push(&newCells, Cell_mitosis(&cells->ptr[i]));
				vec_push(&newCells, Cell_mitosis(&cells->ptr[i]));
			}
		}
	}
	for (size_t i = 0; i < newCells.length; ++i)
	{
		vec_push(cells, newCells.ptr[i]);
	}
	vec_deinit(&newCells);
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(400, 400, "Mitosis");
	Cells cells;
	vec_init(&cells);
	Cell cell;
	Cell_init(
	        &cell,
	        CLITERAL(Vector2){
	                random_between(0, GetScreenWidth()),
	                random_between(0, GetScreenHeight()),
	        },
	        80,
	        CLITERAL(Color){
	                random_between(100, 255),
	                0,
	                random_between(100, 255),
	                100,
	        });
	vec_push(&cells, cell);
	double timer = 0;

	while (!WindowShouldClose())
	{
		timer += GetFrameTime();
		if (timer >= 1.0 / 60.0)
		{
			for (size_t i = 0; i < cells.length; ++i)
			{
				Cell_move(&cells.ptr[i]);
			}
			timer = 0;
		}
		HandleMouse(&cells);
		BeginDrawing();
		ClearBackground(CLITERAL(Color){51, 51, 51, 255});
		for (size_t i = 0; i < cells.length; ++i)
		{
			Cell_show(&cells.ptr[i]);
		}
		EndDrawing();
	}

	vec_deinit(&cells);
	CloseWindow();
}
