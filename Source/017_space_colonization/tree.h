#pragma once

#include <common/vec.h>

#include "branch.h"
#include "leaf.h"

#define MAX_DIST 500
#define MIN_DIST 10

typedef vec(Leaf) LeafVec;
typedef vec(Branch) BranchVec;

typedef struct Tree
{
	LeafVec leaves;
	BranchVec branches;
} Tree;

void Tree_init(Tree* self);
void Tree_show(Tree* self);
