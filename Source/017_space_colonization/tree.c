#include "tree.h"

#include "common/vector2.h"

void Tree_init(Tree* self)
{
	vec_init(&self->leaves);
	for (int i = 0; i < 500; ++i)
	{
		Leaf leaf;
		Leaf_init(&leaf);
		vec_push(&self->leaves, leaf);
	}

	Vector2 pos = {
	        (double)GetScreenWidth() / 2,
	        (double)GetScreenHeight() / 2,
	};
	Vector2 dir = {0, -1};
	Branch root;
	Branch_init(&root, pos, NULL, dir);
	vec_init(&self->branches);
	vec_push(&self->branches, root);

	bool found = false;
	Branch current = root;
	for (int i = 0; i < self->leaves.length; ++i)
	{
		double d = Vector2_dist(current.pos, self->leaves.ptr[i].pos);
		if (d < MAX_DIST)
		{
			found = true;
		}
	}
}

void Tree_show(Tree* self)
{
	for (int i = 0; i < self->leaves.length; ++i)
	{
		Leaf_show(&self->leaves.ptr[i]);
	}
}
