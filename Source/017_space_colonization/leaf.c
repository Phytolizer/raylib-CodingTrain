#include "leaf.h"

#include <common/random.h>

void Leaf_init(Leaf* self)
{
	self->pos = (Vector2){
	        random_between(0, GetScreenWidth()),
	        random_between(0, GetScreenHeight()),
	};
}

void Leaf_show(Leaf* self)
{
	DrawCircleV(self->pos, 2.5, WHITE);
}
