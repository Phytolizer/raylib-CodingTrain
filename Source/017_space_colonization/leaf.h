#pragma once

#include <raylib.h>

typedef struct Leaf
{
	Vector2 pos;
} Leaf;

void Leaf_init(Leaf* self);
void Leaf_show(Leaf* self);
