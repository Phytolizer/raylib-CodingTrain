#include "branch.h"

#include "raylib.h"

void Branch_init(Branch* self, Vector2 pos, Branch* parent, Vector2 dir)
{
	self->pos = pos;
	self->parent = parent;
	self->dir = dir;
}
