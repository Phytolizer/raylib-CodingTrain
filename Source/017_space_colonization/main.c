#include <raylib.h>

#include "tree.h"

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	InitWindow(400, 400, "Fractal Trees - Space Colonization");
	Tree tree;
	Tree_init(&tree);

	while (!WindowShouldClose())
	{
		BeginDrawing();
		ClearBackground((Color){51, 51, 51, 255});
		Tree_show(&tree);
		EndDrawing();
	}

	CloseWindow();
}
