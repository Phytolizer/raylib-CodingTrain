#pragma once

#include <raylib.h>
typedef struct Branch
{
	Vector2 pos;
	struct Branch* parent;
	Vector2 dir;
} Branch;

void Branch_init(Branch* self, Vector2 pos, Branch* parent, Vector2 dir);
