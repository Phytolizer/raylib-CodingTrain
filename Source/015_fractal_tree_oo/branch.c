#include "branch.h"

#include <math.h>

#include <common/random.h>
#include <common/vector2.h>

#ifndef M_PI
#	define M_PI 3.141592653589793
#endif

void Branch_init(Branch* self, Vector2 begin, Vector2 end)
{
	self->begin = begin;
	self->end = end;
}

void Branch_show(Branch* self)
{
	DrawLineV(self->begin, self->end, WHITE);
}

Branch Branch_branchA(Branch* self)
{
	Vector2 dir = Vector2_sub(self->begin, self->end);
	dir = Vector2_rotate(dir, M_PI / 4);
	dir = Vector2_scale(dir, 0.67);
	Vector2 newEnd = Vector2_add(self->end, dir);
	Branch b;
	Branch_init(&b, self->end, newEnd);
	return b;
}

Branch Branch_branchB(Branch* self)
{
	Vector2 dir = Vector2_sub(self->begin, self->end);
	dir = Vector2_rotate(dir, -M_PI / 4);
	dir = Vector2_scale(dir, 0.67);
	Vector2 newEnd = Vector2_add(self->end, dir);
	Branch b;
	Branch_init(&b, self->end, newEnd);
	return b;
}

void Branch_jitter(Branch* self)
{
	self->end.x += random_between(-1, 1);
	self->end.y += random_between(-1, 1);
}
