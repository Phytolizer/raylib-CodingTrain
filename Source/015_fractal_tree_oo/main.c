#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#include <common/vec.h>

#include "branch.h"
#include "leaf.h"

#define NK_IMPLEMENTATION
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#include <nuklear.h>
#include <raylib.h>

typedef vec(Branch) BranchVec;
typedef vec(Leaf) LeafVec;

#ifndef M_PI
#	define M_PI 3.141592653589793
#endif

float dummy_text_width_calculation(
        nk_handle handle,
        float height,
        const char* text,
        int length)
{
	return 1;
}

int main()
{
	SetConfigFlags(FLAG_MSAA_4X_HINT);
	SetConfigFlags(FLAG_VSYNC_HINT);
	InitWindow(600, 600, "Fractal Trees - Object-Oriented");
	struct nk_context ctx;
	struct nk_user_font font;
	font.userdata = (nk_handle){0};
	font.width = dummy_text_width_calculation;
	font.height = 10;
	float sliderValue;
	nk_init_default(&ctx, &font);
	double timer = 0;

	BranchVec tree;
	vec_init(&tree);

	Vector2 a = {300, 600};
	Vector2 b = {300, 500};
	Branch root;
	Branch_init(&root, a, b);
	vec_push(&tree, root);

	LeafVec leaves;
	vec_init(&leaves);

	int branchDepth = 0;

	while (!WindowShouldClose())
	{
		// nuklear window definition
		if (nk_begin(&ctx, "", (struct nk_rect){0, 0, 600, 70}, 0))
		{
			nk_layout_row_dynamic(&ctx, NK_DYNAMIC, 1);
			nk_slider_float(&ctx, 0, &sliderValue, 2 * M_PI, 0.1);
		}
		nk_end(&ctx);

		// Input forwarding to nuklear
		{
			Vector2 mouse = GetMousePosition();
			bool lmbDown = IsMouseButtonDown(MOUSE_LEFT_BUTTON);
			nk_input_begin(&ctx);
			nk_input_motion(&ctx, mouse.x, mouse.y);
			nk_input_button(&ctx, NK_BUTTON_LEFT, mouse.x, mouse.y, lmbDown);
			nk_input_end(&ctx);
		}
		// Input handling
		if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			if (branchDepth < 5)
			{
				for (int64_t i = tree.length - 1; i >= 0; --i)
				{
					if (!tree.ptr[i].finished)
					{
						vec_push(&tree, Branch_branchA(&tree.ptr[i]));
						vec_push(&tree, Branch_branchB(&tree.ptr[i]));
						tree.ptr[i].finished = true;
					}
				}
				++branchDepth;
			}
			else if (branchDepth == 5)
			{
				for (size_t i = 0; i < tree.length; ++i)
				{
					if (!tree.ptr[i].finished)
					{
						Leaf leaf;
						Leaf_init(&leaf, tree.ptr[i].end);
						vec_push(&leaves, leaf);
					}
				}
				++branchDepth;
			}
		}
		// Timer handling
		if (timer > 1.0 / 60.0)
		{
			timer = 0;
			for (size_t i = 0; i < tree.length; ++i)
			{
				// Branch_jitter(&tree.ptr[i]);
			}
		}
		timer += GetFrameTime();
		BeginDrawing();
		ClearBackground((Color){51, 51, 51, 255});

		// regular drawing
		for (size_t i = 0; i < tree.length; ++i)
		{
			Branch_show(&tree.ptr[i]);
		}
		for (size_t i = 0; i < leaves.length; ++i)
		{
			Leaf_show(&leaves.ptr[i]);
		}

		// nuklear drawing
		const struct nk_command* cmd = NULL;
		nk_foreach(cmd, &ctx)
		{
			switch (cmd->type)
			{
				case NK_COMMAND_NOP:
					break;
				case NK_COMMAND_RECT_FILLED:
					{
						const struct nk_command_rect_filled* rect
						        = (const struct nk_command_rect_filled*)cmd;
						DrawRectangle(
						        rect->x,
						        rect->y,
						        rect->w,
						        rect->h,
						        (Color){rect->color.r,
						                rect->color.g,
						                rect->color.b,
						                rect->color.a});
					}
					break;
				case NK_COMMAND_CIRCLE_FILLED:
					{
						const struct nk_command_circle_filled* circle
						        = (const struct nk_command_circle_filled*)cmd;
						DrawEllipse(
						        circle->x + circle->w / 2,
						        circle->y + circle->h / 2,
						        (int)(circle->w / 2),
						        (int)(circle->h / 2),
						        (Color){circle->color.r,
						                circle->color.g,
						                circle->color.b,
						                circle->color.a});
					}
					break;
			}
		}
		DrawFPS(0, 580);
		EndDrawing();
		nk_clear(&ctx);
	}

	vec_deinit(&tree);
	vec_deinit(&leaves);
	nk_free(&ctx);
	CloseWindow();
	return EXIT_SUCCESS;
}
