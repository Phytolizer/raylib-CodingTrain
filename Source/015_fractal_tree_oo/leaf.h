#pragma once

#include <raylib.h>

typedef struct Leaf
{
	Vector2 pos;
} Leaf;

void Leaf_init(Leaf* self, Vector2 pos);
void Leaf_show(Leaf* self);
