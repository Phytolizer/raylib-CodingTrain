#pragma once

#include <raylib.h>

typedef struct
{
	Vector2 begin;
	Vector2 end;
	bool finished;
} Branch;

void Branch_init(Branch* self, Vector2 begin, Vector2 end);
void Branch_show(Branch* self);
Branch Branch_branchA(Branch* self);
Branch Branch_branchB(Branch* self);
void Branch_jitter(Branch* self);
