#include "leaf.h"

#include <raylib.h>

#define PURPLISH \
	CLITERAL(Color) \
	{ \
		255, 0, 100, 100 \
	}

void Leaf_init(Leaf* self, Vector2 pos)
{
	self->pos = pos;
}

void Leaf_show(Leaf* self)
{
	DrawCircleV(self->pos, 4, PURPLISH);
}
